#include<iostream>
#include <fstream>
#include <map>
#include <deque>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>


// you need these includes for the function
//#include <windows.h> // for windows systems
#include <dirent.h> // for linux systems
#include <sys/stat.h> // for linux systems
#include <algorithm>    // std::sort
#include <opencv2/bgsegm.hpp>
#include <string>

using namespace std;

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


int readMarks(std::string filename, std::map<int, int>& msecs) {
    std::string num, time, line;
    int val, coma;
    int maxFrame = 0;
    std::ifstream file(filename, std::ios::in);
    if(file.is_open()) {
        while (std::getline(file, line)) {
            trim(line);
            if(line == "")
                continue;
            coma = line.find(",");
            num = line.substr(0, coma);
            trim(num);
            time = line.substr(coma+1);
            trim(time);
            val = std::stoi(num);
            msecs[val] = std::stoi(time);
            if(val > maxFrame)
                maxFrame = val;
        }
        file.close();
    }
    return maxFrame;
}



double IoU(cv::Rect &r1, cv::Rect &r2) {
    int maxx = r1.x + r1.width, maxy = r1.y + r1.height;
    cv::Rect inter = r1 & r2;
    cv::Mat im = cv::Mat::zeros(maxy, maxx, CV_8UC1);
    cv::rectangle(im, r1, cv::Scalar(255), -1);
    cv::rectangle(im, r2, cv::Scalar(255), -1);
    float inter_area = inter.area(), union_area = cv::countNonZero(im);
    return inter_area/union_area;
}

void readGTBoundingBoxes(std::map<int, cv::Rect> &gt_bboxes) {
    std::string filename = "annotate.csv";
    std::string sframe, sx, sy, sw, sh, line;
    cv::Rect r;
    int frame, coma;
    std::ifstream file(filename, std::ios::in);
    if(file.is_open()) {
        while (std::getline(file, line)) {
            trim(line);
            if(line == "")
                continue;
            coma = line.find(",");
            sframe = line.substr(0, coma);
            trim(sframe);
            frame = std::stoi(sframe);
            line = line.substr(coma+1);
            coma = line.find(",");
            sx = line.substr(0, coma);
            trim(sx);
            r.x = std::stoi(sx);
            line = line.substr(coma+1);
            coma = line.find(",");
            sy = line.substr(0, coma);
            trim(sy);
            r.y = std::stoi(sy);
            line = line.substr(coma+1);
            coma = line.find(",");
            sw = line.substr(0, coma);
            trim(sw);
            r.width = std::stoi(sw);
            sh = line.substr(coma+1);
            trim(sh);
            r.height = std::stoi(sh);

            gt_bboxes[frame] = r;
        }
        file.close();
    }

}

void evaluateResult(std::map<int, cv::Rect> &algorithm_bboxes,
                    std::map<int, cv::Rect> &gt_bboxes) {

    if(algorithm_bboxes.size() != gt_bboxes.size()) {
        std::cout << "Error: The number of bounding boxes for algorithm and groundtruth shall be the same, as they represent the number of frames of the sequence!" << std::endl;
        return;
    }

    std::map<int, cv::Rect>::iterator iter = gt_bboxes.begin();
    std::vector<float> IoUs;

    float vIoU, mean_IoU = 0.0, sd_IoU = 0, min = 2.0, max = -1.0;
    for(; iter != gt_bboxes.end(); iter++) {
        if(algorithm_bboxes.count(iter->first) == 0)
            std::cout << "Error: Entry not found for frame " << iter->first
                      << "in algorithm bounding boxes map!"  << std::endl;

        vIoU = IoU(algorithm_bboxes[iter->first], iter->second);
        if(vIoU < min)
            min = vIoU;
        if(vIoU > max)
            max = vIoU;
        mean_IoU += vIoU;
        IoUs.push_back(vIoU);
        //std::cout << "\tFor frame " << iter->first << " in groundtruth, IoU is: " << vIoU << std::endl;
    }
    mean_IoU /= gt_bboxes.size();
    std::cout << "\tMin IoU: " << min << std::endl;
    std::cout << "\tMax IoU: " << max << std::endl;
    std::cout << "\tMean IoU: " << mean_IoU << std::endl;

    for(uint i=0; i<IoUs.size(); ++i)
        sd_IoU += (IoUs[i] - mean_IoU)*(IoUs[i] - mean_IoU);
    sd_IoU /= gt_bboxes.size() - 1;
    sd_IoU = sqrt(sd_IoU);
    std::cout << "\tStandard Deviation IoU: " << sd_IoU << std::endl;


}

/* FUNCIONES AUXILIARES */

void paintRectangles(cv::Mat &img, std::map<int, cv::Rect> &bboxes) {
    std::map<int, cv::Rect>::iterator it, it_end = bboxes.end();
    for(it = bboxes.begin(); it != it_end; it++) {
        cv::rectangle(img, it->second, cv::Scalar(0,0,255), 2);
    }
}

//busca los minimos y los maximos en las posiciones de los pixeles, en base a eso generar
//luego una box englobante que agrupe esos pixeles
void getBlobs(cv::Mat labels, std::map<int, cv::Rect> &bboxes) {
    int r = labels.rows, c = labels.cols;
    int label, x, y;
    //cout << r <<  ";" << c << ";" << label  << ";" << x << ";" << y << endl;
    bboxes.clear();
    for(int j=0; j<r; ++j) //recorre todos los rows
        for(int i=0; i<c; ++i) { //recorre todos los labels
            label = labels.at<int>(j,i);
            if(label > 0) {
                if(bboxes.count(label) == 0) { //New label
                    cv::Rect r(i,j,1,1);
                    bboxes[label] = r;
                } else { //Update rect
                    cv::Rect &r = bboxes[label];
                    x = r.x + r.width  - 1;
                    y = r.y + r.height - 1;
                    if(i < r.x) r.x = i;
                    if(i > x) x = i;
                    if(j < r.y) r.y = j;
                    if(j > y) y = j;
                    r.width = x - r.x + 1;
                    r.height = y - r.y + 1;
                    //cout << r.x <<  ";" << r.y << ";" << r.y + r.height - 1 << ";" << r.y + r.height - 1 << endl;
                }
            }
        }
}

/* FUNCIONES PARA EL PROCESADO */
// (frame, x, y, width, height)
void searchMaxRect(int actualframe, std::map<int, cv::Rect> &bboxes) {
    int areaMax = 0, indexMax= 1, index = 1, area = 0;
    int frame, x, y, width, height;

    fstream fout;
    fout.open("reportcard.csv", ios::out | ios::app);

    std::map<int, cv::Rect>::iterator it, it_end = bboxes.end();
    for(it = bboxes.begin(); it != it_end; it++) {
        area = it->second.height * it->second.width;
        //cout << it->second.x << ";" << it->second.y << ";" << it->second.height << ";" << it->second.width << "; area:" << area << endl;

        if (areaMax < area) {
            indexMax = index;
            areaMax = area;
            x = it->second.x;
            y = it->second.y;
            height = it->second.height;
            width = it->second.width;
        }
        index++;
    }
    if (width <= 0){
        cout << "No hay movimiento significativo (width) en " << actualframe << ", lea bien el csv que indica cuando hay movimiento" << endl;
        width = 1;
    }
    if (height <= 0){
        cout << "No hay movimiento significativo (height) en " << actualframe << ", lea bien el csv que indica cuando hay movimiento" << endl;
        height = 1;
    }

    fout << actualframe << "," << x << "," << y << "," << width << "," << height << endl;
    //cout << "area:" << areaMax << " index: " << indexMax << endl;
    //cout << "x:" << x << " y:" << y << " height:" << height << " width:" << width << endl;
    fout.close();
}

//lo mismo que lo anterior, pero retemenos el ultimo box
void lastSearchMaxRect(int actualframe, std::map<int, cv::Rect> &bboxes, int *xF, int *yF, int *widthF, int *heightF) {
    int areaMax = 0, indexMax= 1, index = 1, area = 0;
    int frame, x, y, width, height;

    fstream fout;
    fout.open("reportcard.csv", ios::out | ios::app);

    std::map<int, cv::Rect>::iterator it, it_end = bboxes.end();
    for(it = bboxes.begin(); it != it_end; it++) {
        area = it->second.height * it->second.width;
        //cout << it->second.x << ";" << it->second.y << ";" << it->second.height << ";" << it->second.width << "; area:" << area << endl;

        if (areaMax < area) {
            indexMax = index;
            areaMax = area;
            x = it->second.x;
            y = it->second.y;
            height = it->second.height;
            width = it->second.width;
        }
        index++;
    }

    fout << actualframe << "," << x << "," << y << "," << width << "," << height << endl;
    //guardar el ultimo box antes de detener el movimiento
    *xF = x;
    *yF = y;
    *widthF = width;
    *heightF = height;
    //cout << "area:" << areaMax << " index: " << indexMax << endl;
    //cout << "x:" << x << " y:" << y << " height:" << height << " width:" << width << endl;
    fout.close();
}

// hubiese modificado la anterior, pero cumplo con no modificar el codigo entregado por el profesor
void readMyGTBoundingBoxes(std::map<int, cv::Rect> &gt_bboxes) {
    std::string filename = "reportcard.csv";
    std::string sframe, sx, sy, sw, sh, line;
    cv::Rect r;
    int frame, coma;
    std::ifstream file(filename, std::ios::in);
    if(file.is_open()) {
        while (std::getline(file, line)) {
            trim(line);
            if(line == "")
                continue;
            coma = line.find(",");
            sframe = line.substr(0, coma);
            trim(sframe);
            frame = std::stoi(sframe);
            line = line.substr(coma+1);
            coma = line.find(",");
            sx = line.substr(0, coma);
            trim(sx);
            r.x = std::stoi(sx);
            line = line.substr(coma+1);
            coma = line.find(",");
            sy = line.substr(0, coma);
            trim(sy);
            r.y = std::stoi(sy);
            line = line.substr(coma+1);
            coma = line.find(",");
            sw = line.substr(0, coma);
            trim(sw);
            r.width = std::stoi(sw);
            sh = line.substr(coma+1);
            trim(sh);
            r.height = std::stoi(sh);

            gt_bboxes[frame] = r;
        }
        file.close();
    }
}


/* Seccion de gestion de lectura del CSV
 *
 * Llamar a framesWithMovement, sustituira los dos int que le entreguen por los frames asociados
 * al inicio o al fin del movimiento. Si alguno de los dos es 0, implica que se ingreso un
 * tiempo que exede lo posible a leer en el csv
 */
vector<vector<double>> parse2DCsvFile(string inputFileName) {

    vector<vector<double> > data;
    ifstream inputFile(inputFileName);
    int l = 0;

    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;

            while (ss) {
                string line;
                if (!getline(ss, line, ','))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << inputFileName << " line " << l
                         << endl;
                    e.what();
                }
            }

            data.push_back(record);
        }
    }

    if (!inputFile.eof()) {
        cerr << "Could not read file " << inputFileName << "\n";
        __throw_invalid_argument("File not found.");
    }

    return data;
}

void framesWithMovement( int move_initTime, int move_endTime, int *Minit, int *Mend ){
    vector<vector<double>> data = parse2DCsvFile("marks.csv");

    int move_initframe = 0; //el enunciado dice desde el tiempo 2000 pero no es así
    int move_endframe = 0; //el enunciado dice hasta el tiempo 16000 pero no es así
    int conteo = 0;

    int actual_data = 1, actual_frame = 0, actual_time = 0;
    for (auto l : data) {
        for (auto x : l) { //el primero es el frame, el segundo el tiempo
            if (actual_data == 2){ //extraer milegundos de el frame
                actual_data = 1;
                actual_time = x;

                //desde el frame anterior a este tiempo tendremos mov
                if ( (actual_time > move_initTime) && (move_initframe == 0) ) {
                    move_initframe = actual_frame;
                }
                //desde el frame anterior a este tiempo tendremos mov
                if ( (actual_time > move_endTime) && (move_endframe == 0) && (move_initframe > 0) ) {
                    move_endframe = actual_frame;
                }
            }
            if (actual_data == 1){ //extraer frame
                actual_data = 2;
                actual_frame = x;
            }
        }
        conteo++;
    }

    if (move_endframe == 0) move_endframe = conteo; //proteccion si exedimos el tiempo

    *Minit = move_initframe;
    *Mend = move_endframe;

    cout << "El csv indica que el frame de movimiento inicial es: " <<move_initframe << endl;
    cout << "El csv indica que el frame de movimiento final es: " <<move_endframe << endl;
}

/* MAIN */
int main(int argc, const char **argv) {

    std::map<int, cv::Rect> gt_bboxes;
    std::map<int, cv::Rect> alg_bboxes; //Guarde las cajas englobantes por frame aca.

    readGTBoundingBoxes(gt_bboxes); // Lee la groundtruth y la almacena en gt_bboxes.

    //WRITE YOUR CODE HERE!!
    /*
     * COMENTARIOS
     * La estrategia es obtener utilizar MoG sobre los frames donde el csv indique que existe movimiento,
     * que lo hace bastante bien. Problema: no encontre solucion al problema de identificacion de la persona mientras
     * estaba quieta, de manera dinamica. Mi plan era contar los pixeles con un histograma para lograr identificar al sujeto en
     * cualquier lugar que fuese el punto de inicio de la persona (segmentarlo simplemente encontrado su color fue inutil),
     * pero estoy intuyendo que eso no es correcto, ya que el codigo saldria tremendamente largo para la pregunta.
     *
     * En lugar de ensuciar mi codigo con algo que intuyo estoy suponiendo mal, entregare una anexo a esta pregunta,
     * <<44_counting_pixels2>>, que es mi estrategia para encontrar al sujeto con los histogramas, el cual no esta 100%
     * como yo preferiría implementarlo ya que falta adaptabilidad en el tamano del box que encierra al sujeto (si esta
     * mas cerca debiese dinamicamente encerrarle).
     *
     * Para correr esto basta:
     * Configurar el working directory en la carpeta del proyecto
     * configurar la carpeta donde se encuentra la secuencia (SEQUENCE) en cv::videocapture
     *
     * Creo entender que el csv me indica donde comienza/termina el movimiento de el video, pero si seteo
     * 2000 y 16000 como umbrales de fin/inicio, claramente no coinciden con los frames. El movimiento es
     * muy significativo desde los 500. Este parametro puede setearse en la función:
     * framesWithMovement(500, 20000, &Minit, &Mend );
     *
     * No pregunte este detalle por discord, ya que no se si violare alguna norma sobre el certamen.
     *
     */

    /* LECTURA DE ARCHIVOS */

    //eliminamos registro anterior, si es que existe.
    if( remove( "reportcard.csv" ) != 0 ) perror( "Eliminado archivo antiguo" );
    else puts( "Se creara archivo reportcard.csv donde se guardarán los resultados de los boxes para cada frame" );

    // abrimos el archivo de video (CAMBIAR AQUI)
    cv::VideoCapture capture("/home/laila/programacion/C++/openCV/CV/CV2021_C1_P2/SEQUENCE/%6d.png");

    // Chequeamos que este correctamente abierto
    if (!capture.isOpened())
        return 0;

    // current video frame
    cv::Mat frame;
    // foreground binary image
    cv::Mat foreground;
    // background image
    cv::Mat background;
    //labels
    cv::Mat labels;

    cv::namedWindow("Extracted Foreground");

    //configuracion de los parametros del MoG
    cv::Ptr<cv::BackgroundSubtractorMOG2> ptrMOG;// The Mixture of Gaussian object
    int historyMOG = 500; //Length of the history, no hace nada XD
    int nmixtures = 75; //Number of Gaussians Mixtures, numero de mezclas
    //que % de los modelos se considerara fondo
    //Threshold of data that should be accounted for by the background (Parámetro T)
    double backgroundRatio = 0.3;
    double learningRate = 0.020; //Learning Rate
    double varThreshold = 155; //Umbral segmentacion, umbral de la distancia 105
    bool bShadowDetection = false; //quiere o no detectar sombras - falla a veces
    ptrMOG = cv::createBackgroundSubtractorMOG2(historyMOG, varThreshold, bShadowDetection);
    ptrMOG->setNMixtures(nmixtures);
    ptrMOG->setBackgroundRatio(backgroundRatio);

    //leemos el csv para obtener los frames donde inicia y termina el movimiento
    //2000 no coincide con el inicio del movimiento y 16000 no coincide con el fin del movimiento, se mueve siempre!
    //existe movimiento desde 500-600 aproximadamente
    int Minit = 0, Mend = 0;
    framesWithMovement(500, 20000, &Minit, &Mend );

    //controles y conteos del procesado de todo el video
    bool stop(false);
    bool first = true;
    int esize = 5; //tamaño filtro morfologico
    int xF = 0, yF = 0, widthF = 0, heightF = 0; //guardaremos el ultimo movimiento
    int actualprocessedframes = 1, frametoprocess = 700;


    // para todos los frames del video usaremos MoG. En teoria deberia utilizar la informacion del csv aqui
    while (!stop) {

        // read next frame if any
        if (!capture.read(frame))
            break;

        if (first) {
            first = false;
            frame.copyTo(background);
        }

        ptrMOG->apply(frame, foreground, learningRate);

        //apertura y cierre morfologico para limpiar
        cv::erode(foreground, foreground, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));
        cv::dilate(foreground, foreground, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));

        cv::dilate(foreground, foreground, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));
        cv::erode(foreground, foreground, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));


        cv::connectedComponents(foreground, labels, 8, CV_32S);
        std::map<int, cv::Rect> bboxes;
        getBlobs(labels, bboxes); //genera los blobs
        paintRectangles(frame, bboxes);

        //Gestion de el movimento
        if ( actualprocessedframes < Minit ) { //inicialmente parado
            fstream fout;
            fout.open("reportcard.csv", ios::out | ios::app);
            fout << actualprocessedframes << "," << 295 << "," << 52 << "," << 51 << "," << 144 << endl; //posicion inicial conocida, ver comentarios
            fout.close();
        }
        if ( (Minit <= actualprocessedframes) && (actualprocessedframes < Mend) ) { //en movimiento
            searchMaxRect(actualprocessedframes, bboxes);
        }
        if ( actualprocessedframes == Mend ) { //ultimo frame en movimiento
            lastSearchMaxRect(actualprocessedframes, bboxes, &xF, &yF, &widthF, &heightF);
        }
        if ( Mend < actualprocessedframes) { //fin parado
            fstream fout;
            fout.open("reportcard.csv", ios::out | ios::app);
            fout << actualprocessedframes << "," << xF << "," << yF << "," << widthF << "," << heightF << endl; //posicion inicial conocida, ver comenta
            fout.close();
        }

        imshow("Video", frame);
        cv::imshow("Extracted Foreground", foreground);

        // introduce a delay or press key to stop
        if (cv::waitKey(5) >= 0)
            stop = true;
        if (actualprocessedframes == frametoprocess) {
            stop = true;
        }
        actualprocessedframes++;
    }

    cv::waitKey();



    /* PRUEBA DE RENDIMIENTO
     * Prueba de rendimiento del algoritmo
     * */
    readMyGTBoundingBoxes(alg_bboxes);
    evaluateResult(alg_bboxes, gt_bboxes);

    return 0;
}
