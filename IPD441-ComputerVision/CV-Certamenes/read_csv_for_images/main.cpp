#include <string>
#include <vector>
#include <sstream> //istringstream
#include <iostream> // cout
#include <fstream> // ifstream

using namespace std;

/**
 * Reads csv file into table, exported as a vector of vector of doubles.
 * @param inputFileName input file name (full path).
 * @return data as vector of vector of doubles.
 */
vector<vector<double>> parse2DCsvFile(string inputFileName) {

    vector<vector<double> > data;
    ifstream inputFile(inputFileName);
    int l = 0;

    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;

            while (ss) {
                string line;
                if (!getline(ss, line, ','))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << inputFileName << " line " << l
                         << endl;
                    e.what();
                }
            }

            data.push_back(record);
        }
    }

    if (!inputFile.eof()) {
        cerr << "Could not read file " << inputFileName << "\n";
        __throw_invalid_argument("File not found.");
    }

    return data;
}

void framesWithMovement( int move_initTime, int move_endTime, int *Minit, int *Mend ){
    vector<vector<double>> data = parse2DCsvFile("marks.csv");

    int move_initframe = 0; //el enunciado dice desde el tiempo 2000 pero no es así
    int move_endframe = 0; //el enunciado dice hasta el tiempo 16000 pero no es así
    int conteo = 0;

    int actual_data = 1, actual_frame = 0, actual_time = 0;
    for (auto l : data) {
        for (auto x : l) { //el primero es el frame, el segundo el tiempo
            if (actual_data == 2){ //extraer milegundos de el frame
                actual_data = 1;
                actual_time = x;

                //desde el frame anterior a este tiempo tendremos mov
                if ( (actual_time > move_initTime) && (move_initframe == 0) ) {
                    move_initframe = actual_frame;
                }
                //desde el frame anterior a este tiempo tendremos mov
                if ( (actual_time > move_endTime) && (move_endframe == 0) && (move_initframe > 0) ) {
                    move_endframe = actual_frame;
                }
            }
            if (actual_data == 1){ //extraer frame
                actual_data = 2;
                actual_frame = x;
            }
        }
        conteo++;
    }
    cout << move_initframe << endl;
    cout << move_endframe << endl;

    if (move_endframe == 0) move_endframe = conteo;

    *Minit = move_initframe;
    *Mend = move_endframe;
}


int main()
{

    int Minit = 0, Mend = 0;
    framesWithMovement(2000, 35000, &Minit, &Mend );

    cout << Minit << endl;
    cout << Mend << endl;

    return 0;
}