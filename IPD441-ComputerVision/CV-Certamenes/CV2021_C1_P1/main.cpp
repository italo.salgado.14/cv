#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <map>
#include<iostream>
#include <iomanip>

using namespace std;

//Generate image with 2D histogram representation from previously calculated histogram (histo):
cv::Mat gen2DHistogramImage(const cv::Mat &histo, std::string c1_label = "X1",
                            std::string c2_label = "X2", bool with_gs = false, cv::Mat *gs_histo=NULL) {
    int i, j, bins = histo.cols, bsize = 256/bins, bin_val;
    int iwidth = 1000, iheight = 1000, border = 10, text_hspace = 150, text_vspace = 80, intercell = 5;
    int wcells_zone = iwidth - 2*border - text_hspace, hcells_zone = iheight - 2*border - text_vspace;
    int baseline,
        wcell = (wcells_zone - intercell*(bins-1))/bins,
        hcell = (hcells_zone - intercell*(bins-1))/bins;
    cv::Scalar bgColor(255, 255, 255);
    cv::Scalar axColor(0, 0, 0);
    cv::Scalar cellBorderColor(128, 128, 128);
    cv::Mat colorsBGR(bins, bins, CV_8UC3), gs_view(bins, bins, CV_8UC1);
    cv::Mat norm_histo(bins, bins, CV_32FC1);

    cv::Mat r(iheight, iwidth, CV_8UC3, bgColor);

    for(i=0; i<bins; ++i)
        for(j=0; j<bins; ++j)
            norm_histo.at<float>(i,j) = histo.at<float>(i,j);
    double vmin, vmax;
    float fval;
    cv::minMaxLoc(norm_histo, &vmin, &vmax);

    //Get colors
    uchar gs_level;
    uchar *data = gs_view.data;
    int step = gs_view.step;
    for(i=0; i<bins; ++i)
        for(j=0; j<bins; ++j) {
            fval = (norm_histo.at<float>(i,j) - vmin)/(vmax - vmin);
            gs_level = (fval*255 > 255) ? 255 : (fval*255 < 0? 0: (uchar)(fval*255));

            data[i*step + j] = gs_level;
        }
    cv::applyColorMap(gs_view, colorsBGR, cv::COLORMAP_JET);

    //Bins
    step = colorsBGR.step;
    data = colorsBGR.data;
    uchar B, G, R;
    int x, y;
    for(i=0; i<bins; ++i)
        for(j=0; j<bins; ++j) {
            B = data[i*step + j*3    ];
            G = data[i*step + j*3 + 1];
            R = data[i*step + j*3 + 2];
            std::vector<cv::Point> points;
            std::vector<std::vector<cv::Point> > ppoints;
            x = border+text_hspace+(intercell+wcell)*j;
            y = border+text_vspace+(intercell+hcell)*i;
            points.push_back(cv::Point(x, y));
            points.push_back(cv::Point(x + wcell, y));
            points.push_back(cv::Point(x + wcell, y + hcell));
            points.push_back(cv::Point(x, y + hcell));
            ppoints.push_back(points);
            cv::fillPoly(r, ppoints, cv::Scalar(B, G, R));
        }

    //Horizontal ax
    cv::line(r, cv::Point(border+text_hspace, border+text_vspace),
                cv::Point(border+text_hspace+wcells_zone, border+text_vspace), axColor, 2);
    cv::Size textSize = cv::getTextSize(c1_label, cv::FONT_HERSHEY_DUPLEX, 1, 1, &baseline);
    cv::putText(r, c1_label,
                cv::Point2i(border+text_hspace+wcells_zone/2-textSize.width/2,
                            border+text_vspace-50),
                cv::FONT_HERSHEY_DUPLEX, 1, axColor);


    for(i=0; i<bins; ++i) {
        cv::line(r, cv::Point(border+text_hspace+wcell/2+(intercell+wcell)*i, border+text_vspace-10),
                    cv::Point(border+text_hspace+wcell/2+(intercell+wcell)*i, border+text_vspace), axColor, 2);
        bin_val = bsize/2 + i*bsize;
        std::string bval_str = std::to_string(bin_val);
        cv::Size textSize = cv::getTextSize(bval_str, cv::FONT_HERSHEY_DUPLEX, 0.8, 1, &baseline);
        cv::putText(r, bval_str,
                    cv::Point2i(border+text_hspace+wcell/2+(intercell+wcell)*i-textSize.width/2,
                                border+text_vspace-15),
                    cv::FONT_HERSHEY_DUPLEX, 0.8, axColor);
    }

    //Vertical ax
    cv::line(r, cv::Point(border+text_hspace, border+text_vspace),
                cv::Point(border+text_hspace, border+text_vspace+hcells_zone), axColor, 2);
    textSize = cv::getTextSize(c2_label, cv::FONT_HERSHEY_DUPLEX, 1, 1, &baseline);
    cv::putText(r, c2_label,
                cv::Point2i(border+text_hspace/2-textSize.width,
                            border+text_vspace+hcells_zone/2),
                cv::FONT_HERSHEY_DUPLEX, 1, axColor);
    for(i=0; i<bins; ++i) {
        cv::line(r, cv::Point(border+text_hspace-10, border+text_vspace+hcell/2+(intercell+hcell)*i),
                    cv::Point(border+text_hspace,    border+text_vspace+hcell/2+(intercell+hcell)*i), axColor, 2);
        bin_val = bsize/2 + i*bsize;
        std::string bval_str = std::to_string(bin_val);
        cv::Size textSize = cv::getTextSize(bval_str, cv::FONT_HERSHEY_DUPLEX, 0.8, 1, &baseline);
        cv::putText(r, bval_str,
                    cv::Point2i(border+text_hspace-textSize.width-10,
                                border+text_vspace+wcell/2+(intercell+hcell)*i+textSize.height/2),
                                cv::FONT_HERSHEY_DUPLEX, 0.8, axColor);
    }

    if(with_gs && gs_histo != NULL) {
        // Draw the histograms for B, G and R
        int hist_w = 512; int hist_h = 400;
        int bin_w = cvRound( (double) hist_w/bins);
        gs_histo->create(hist_h, hist_w, CV_8UC3);
        *gs_histo = cv::Scalar(0,0,0);
        cv::Mat gs(1, bins, CV_32FC1);
        for(i=0; i<bins; ++i)
            gs.at<float>(0, i) = histo.at<float>(bins, i);
        for( int i = 1; i < bins; i++) {
            cv::line(*gs_histo, cv::Point( bin_w*(i-1), hist_h - cvRound(255*gs.at<float>(i-1)) ) ,
                            cv::Point( bin_w*(i), hist_h - cvRound(255*gs.at<float>(i)) ),
                            cv::Scalar(255, 255, 255), 2, 8, 0  );
        }
    }

    return r;
}

//Generate image with 1D histogram representation from previously calculated histogram (histo):
cv::Mat gen1DHistogramImage(const cv::Mat &histo, std::string var_label = "X1") {
    int i, bins = histo.rows, bsize = 256/bins, bin_val;
    int iwidth = 1000, iheight = 1000, border = 20, text_hspace = 200, text_vspace = 80, intercell = 5;
    int wcells_zone = iwidth - 2*border - text_hspace, hcell_zone = iheight - 2*border - text_vspace;
    int baseline,
        wcell = (wcells_zone - intercell*(bins-1))/bins;
    cv::Scalar bgColor(255, 255, 255);
    cv::Scalar axColor(0, 0, 0);
    cv::Scalar cellBorderColor(128, 128, 128);
    cv::Mat colorsBGR(bins, 1, CV_8UC3), gs_view(bins, 1, CV_8UC1);
    cv::Mat norm_histo(bins, 1, CV_32FC1);

    cv::Mat r(iheight, iwidth, CV_8UC3, bgColor);

    for(i=0; i<bins; ++i)
        norm_histo.at<float>(i,0) = histo.at<float>(i,0);
    double vmin, vmax;
    float fval;
    cv::minMaxLoc(norm_histo, &vmin, &vmax);

    //Get colors
    uchar gs_level;
    uchar *data = gs_view.data;
    int step = gs_view.step;
    for(i=0; i<bins; ++i) {
        fval = (norm_histo.at<float>(i,0) - vmin)/(vmax - vmin);
        gs_level = (fval*255 > 255) ? 255 : (fval*255 < 0? 0: (uchar)(fval*255));
        std::cout << "Bin " << i << ": " << (int)gs_level <<std::endl;
        data[i] = gs_level;
    }
    cv::applyColorMap(gs_view, colorsBGR, cv::COLORMAP_JET);

    //Bins
    step = colorsBGR.step;
    data = colorsBGR.data;
    uchar B, G, R;
    int x, y;
    for(i=0; i<bins; ++i) {
        B = data[i*step    ];
        G = data[i*step + 1];
        R = data[i*step + 2];
        std::vector<cv::Point> points;
        std::vector<std::vector<cv::Point> > ppoints;
        x = border+text_hspace+(intercell+wcell)*i;
        y = gs_view.data[i];
        std::cout << "Bin " << i << ": " << (int)y
                  << "("  << (int)B
                  << "; " << (int)G
                  << "; " << (int)R << ")" << std::endl;
        if(y == 0)
            continue;
        points.push_back(cv::Point(x, hcell_zone + border));
        points.push_back(cv::Point(x + wcell, hcell_zone + border));
        points.push_back(cv::Point(x + wcell, hcell_zone + border - y*(hcell_zone)/255.0));
        points.push_back(cv::Point(x, hcell_zone + border - y*(hcell_zone)/255.0));
        ppoints.push_back(points);
        cv::fillPoly(r, ppoints, cv::Scalar(B, G, R));
    }

    //Horizontal ax
    cv::line(r, cv::Point(border+text_hspace, hcell_zone + border),
                cv::Point(border+text_hspace+wcells_zone, hcell_zone + border), axColor, 2);
    cv::Size textSize = cv::getTextSize(var_label, cv::FONT_HERSHEY_DUPLEX, 1, 1, &baseline);
    cv::putText(r, var_label,
                cv::Point2i(border+text_hspace+wcells_zone/2-textSize.width/2,
                            hcell_zone + border + text_vspace/2 + 20),
                cv::FONT_HERSHEY_DUPLEX, 1, axColor);

    for(i=0; i<bins; ++i) {
        cv::line(r, cv::Point(border+text_hspace+wcell/2+(intercell+wcell)*i, hcell_zone + border),
                    cv::Point(border+text_hspace+wcell/2+(intercell+wcell)*i, hcell_zone + border + 10), axColor, 2);
        bin_val = bsize/2 + i*bsize;
        std::string bval_str = std::to_string(bin_val);
        cv::Size textSize = cv::getTextSize(bval_str, cv::FONT_HERSHEY_DUPLEX, 0.6, 1, &baseline);
        cv::putText(r, bval_str,
                    cv::Point2i(border+text_hspace+wcell/2+(intercell+wcell)*i-textSize.width/2,
                                border+hcell_zone+30),
                    cv::FONT_HERSHEY_DUPLEX, 0.6, axColor);
    }

    //Vertical ax
    cv::line(r, cv::Point(border+text_hspace, border),
                cv::Point(border+text_hspace, border + hcell_zone), axColor, 2);
    cv::putText(r, "Frequency",
                cv::Point2i(border/2
                            ,
                            border + hcell_zone/2 - 15),
                cv::FONT_HERSHEY_DUPLEX, 0.7, axColor);
    cv::putText(r, "Scaled(255)",
                cv::Point2i(border/2,
                            border + hcell_zone/2 + 15),
                cv::FONT_HERSHEY_DUPLEX, 0.7, axColor);
    int ival, lines = 256/16+1;

    for(i=0; i<lines; ++i) {
        cv::line(r, cv::Point(border+text_hspace-10, border+hcell_zone-(hcell_zone*i*16)/255),
                    cv::Point(border+text_hspace,    border+hcell_zone-(hcell_zone*i*16)/255),
                 axColor, 2);
        ival = i*16;
        std::string istr = std::to_string(ival);
        cv::Size textSize = cv::getTextSize(istr, cv::FONT_HERSHEY_DUPLEX, 0.8, 1, &baseline);
        cv::putText(r, istr,
                    cv::Point2i(border+text_hspace-textSize.width-15,
                                border+hcell_zone-(hcell_zone*i*16)/255+5),
                                cv::FONT_HERSHEY_DUPLEX, 0.8, axColor);
    }
    return r;
}

//Generate 1D histogram representation from 1-channel image:
cv::Mat histogram1channel(cv::Mat im, int bins) {
    cv::Mat histo(bins, 1, CV_32FC1);
    histo = 0.0;
    int i, j, bsize = 255/bins, counter = 0,
        rows = im.rows, cols = im.cols;
    int step = im.step;
    uchar *data = im.data;
    uchar C;
    for(i=0; i<rows; ++i)
        for(j=0; j<cols; ++j) {
            counter++;
            C = data[i*step + j];
            histo.at<float>(C/bsize, 0) += 1.0;
        }
    histo /= counter;
    return histo;
}

//Generate 2D histogram representation from 3-channel image (choosing channels c1 and c2):
cv::Mat histogram2channels(cv::Mat im, int c1, int c2, int bins) {
    cv::Mat histo(bins, bins, CV_32FC1);
    histo = 0.0;
    int i, j, bsize = 255/bins, counter = 0,
        rows = im.rows, cols = im.cols;
    std::vector<cv::Mat> channels;
    cv::split(im, channels);
    int step = channels[0].step;
    uchar *c1_data = channels[c1].data, *c2_data = channels[c2].data;
    uchar C1, C2;
    for(i=0; i<rows; ++i)
        for(j=0; j<cols; ++j) {
            counter++;
            C1 = c1_data[i*step + j];
            C2 = c2_data[i*step + j];
            histo.at<float>(C1/bsize, C2/bsize) += 1.0;
        }
    histo /= counter;
    return histo;
}

cv::Mat sobelGradient(cv::Mat src) {
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

    /// Generate grad_x and grad_y
    cv::Mat grad, grad_x, grad_y, abs_grad_x, abs_grad_y;

    /// Gradiente X
    cv::Sobel(src, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT);
    cv::convertScaleAbs(grad_x, abs_grad_x);

    /// Gradiente Y
    cv::Sobel(src, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);

    /// Total Gradient (approximate)
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

    double maxVal;
    cv::minMaxLoc(grad, NULL, &maxVal, NULL, NULL);
    double factor = 255/maxVal;

    cv::Mat m8u;

    grad.convertTo(m8u, CV_8U, factor);


    return m8u;
}


float recall(int TP, int FN) {
    if(TP == 0 && FN == 0)
        return 0.0;
    return TP/(float)(TP+FN);
}

float precision(int TP, int FP) {
    if(TP == 0 && FP == 0)
        return 0.0;
    return TP/(float)(TP+FP);
}


float F1_score(float precision, float recall) {
    return 2*precision*recall/(precision+recall);
}

void paintRectangles(cv::Mat &img, std::map<int, cv::Rect> &bboxes) {
    std::map<int, cv::Rect>::iterator it, it_end = bboxes.end();
    for(it = bboxes.begin(); it != it_end; it++) {
        cv::rectangle(img, it->second, cv::Scalar(0,0,255), 2);
    }
}

void getBlobs(cv::Mat labels, std::map<int, cv::Rect> &bboxes) {
    int r = labels.rows, c = labels.cols;
    int label, x, y;
    bboxes.clear();
    for(int j=0; j<r; ++j)
        for(int i=0; i<c; ++i) {
            label = labels.at<int>(j,i);
            if(label > 0) {
                if(bboxes.count(label) == 0) { //New label
                    cv::Rect r(i,j,1,1);
                    bboxes[label] = r;
                } else { //Update rect
                    cv::Rect &r = bboxes[label];
                    x = r.x + r.width  - 1;
                    y = r.y + r.height - 1;
                    if(i < r.x) r.x = i;
                    if(i > x) x = i;
                    if(j < r.y) r.y = j;
                    if(j > y) y = j;
                    r.width = x - r.x + 1;
                    r.height = y - r.y + 1;
                }
            }
        }
}

double IoU(cv::Rect &r1, cv::Rect &r2) {
    int maxx = r1.x + r1.width, maxy = r1.y + r1.height;
    cv::Rect inter = r1 & r2;
    cv::Mat im = cv::Mat::zeros(maxy, maxx, CV_8UC1);
    cv::rectangle(im, r1, cv::Scalar(255), -1);
    cv::rectangle(im, r2, cv::Scalar(255), -1);
    float inter_area = inter.area(), union_area = cv::countNonZero(im);
    return inter_area/union_area;
}

//Bounding box centers distance:
float rectDistance(cv::Rect &r1, cv::Rect &r2) {
    float dx = r1.x + r1.width/2  - r2.x - r2.width/2,
          dy = r1.y + r1.height/2 - r2.y - r2.height/2;
    return sqrt(dx*dx + dy*dy);
}

cv::Rect getNearestFromCenter(cv::Rect &r, std::map<int, cv::Rect> &bboxes) {
    if(bboxes.size() == 0)
        return cv::Rect(0,0,0,0);
    std::map<int, cv::Rect>::iterator iter = bboxes.begin();
    int nearest_index = iter->first;
    float dist, min_dist = rectDistance(r, iter->second);
    for(iter++; iter != bboxes.end(); iter++) {
        dist = rectDistance(r, iter->second);
        if(dist < min_dist) {
            min_dist = dist;
            nearest_index = iter->first;
        }
    }

    return bboxes[nearest_index];
}

void evaluateResult(cv::Mat &eval_mask, cv::Mat &result) {
    cv::Mat mask, bin_mask;
    if(eval_mask.channels() == 3)
        cv::cvtColor(eval_mask, mask, cv::COLOR_BGR2GRAY);
    else if(eval_mask.channels() == 1)
        mask = eval_mask;
    else {
        std::cerr << "Error! La máscara de evaluación debe ser de 1 o 3 canales." << std::endl;
        return;
    }

    if(result.channels() != 1) {
        std::cerr << "Error! La máscara de evaluación debe ser de 1 o 3 canales." << std::endl;
        return;
    }

    if(result.cols != mask.cols || result.rows != mask.rows) {
        std::cerr << "Error! El tamaño de las imágenes de evaluación y resultados debe ser el mismo." << std::endl;
        return;
    }

    cv::threshold(mask, bin_mask, 128, 255, cv::THRESH_BINARY);
    cv::imshow("Eval mask", bin_mask);

    cv::threshold(result, result, 128, 255, cv::THRESH_BINARY);

    cv::Mat labels, stats, centroids;
    cv::connectedComponentsWithStats(bin_mask, labels, stats, centroids);

    std::map<int, cv::Rect> bboxes;
    getBlobs(labels, bboxes);
    cv::Mat color_bmask;
    cv::cvtColor(bin_mask, color_bmask, cv::COLOR_GRAY2BGR);
    paintRectangles(color_bmask, bboxes);
    cv::imshow("Eval mask + Bounding Boxes", color_bmask);

    cv::Mat labels2, stats2, centroids2;
    cv::connectedComponentsWithStats(result, labels2, stats2, centroids2);

    std::map<int, cv::Rect> bboxes2;
    getBlobs(labels2, bboxes2);
    cv::Mat color_result;
    cv::cvtColor(result, color_result, cv::COLOR_GRAY2BGR);
    paintRectangles(color_result, bboxes);
    cv::imshow("Result mask + Bounding Boxes", color_result);

    //Global stats
    int TP = 0, TN = 0, FP = 0, FN = 0;
    cv::Mat color_gstats = cv::Mat::zeros(mask.size(), CV_8UC3);
    int i, j, k, step = mask.step, rows = mask.rows, cols = mask.cols, cstep = color_gstats.step;
    uchar GT, ALG, *mdata = mask.data, *rdata = result.data, *cdata = color_gstats.data;
    for(i=0; i<rows; ++i)
        for(j=0, k=0; j<cols; ++j, k+=3) {
            GT = mdata[i*step + j];
            ALG = rdata[i*step + j];
            if(GT == 255 && ALG == 255) {
                ++TP; //Green for TP
                cdata[i*cstep + k + 1] = 255;
            } else if(GT == 0 && ALG == 255) {
                ++FP; //Red for FP
                cdata[i*cstep + k + 2] = 255;
            } else if(GT == 255 && ALG == 0) {
                ++FN; //Yellow for FN
                cdata[i*cstep + k + 1] = 255;
                cdata[i*cstep + k + 2] = 255;
            } else {
                ++TN; //Black for TN
            }
        }
    std::cout << "GLOBAL STATS:" << std::endl;
    std::cout << "Color stats: GREEN for TP, RED for FP, YELLOW for FN, BLACK for TN." << std::endl;
    std::cout << "\tTP: " << TP << std::endl;
    std::cout << "\tFP: " << FP << std::endl;
    std::cout << "\tFN: " << FN << std::endl;
    std::cout << "\tTN: " << TN << std::endl;
    float prec = precision(TP, FP), rec = recall(TP, FN), fscore = F1_score(prec, rec);
    std::cout << "\tPrecision: " << prec << std::endl;
    std::cout << "\tRecall: " << rec << std::endl;
    std::cout << "\tF1-score: " << fscore << std::endl;
    std::cout << "\tApples in ground-thruth: " << bboxes.size() << std::endl;
    std::cout << "\tApples detected: " << bboxes2.size() << std::endl;
    std::cout << "\tDetection error: " << std::setprecision(2) << fabs(bboxes.size() - bboxes2.size())*100.0/bboxes.size() << "%" << std::endl;

    cv::imshow("Color Stats", color_gstats);

    if(bboxes.size() != 0 && bboxes2.size() == 0) {
        std::cout << "\tThere are " << bboxes.size() << " bounding boxes in groundtruth, but you detected none. Considered as a mean IoU = 0." << std::endl;
    } else {
        std::cout << "\tMean IoU calculated considering the nearest detected bounding box to each ground-truth bounding box." << std::endl;
        std::map<int, cv::Rect>::iterator iter = bboxes.begin();
        std::vector<float> IoUs;
        float vIoU, mean_IoU = 0.0, sd_IoU = 0;
        for(; iter != bboxes.end(); iter++) {
            cv::Rect nearest = getNearestFromCenter(iter->second, bboxes2);
            vIoU = IoU(nearest, iter->second);
            mean_IoU += vIoU;
            IoUs.push_back(vIoU);
            std::cout << "\tFor bounding box " << iter->first << " in groundtruth, IoU is: " << vIoU << std::endl;
        }
        mean_IoU /= bboxes.size();
        std::cout << "\tMean IoU: " << mean_IoU << std::endl;

        for(uint i=0; i<IoUs.size(); ++i)
            sd_IoU += (IoUs[i] - mean_IoU)*(IoUs[i] - mean_IoU);
        sd_IoU /= bboxes.size() - 1;
        sd_IoU = sqrt(sd_IoU);
        std::cout << "\tStandard Deviation IoU: " << sd_IoU << std::endl;
    }

}

/*
 * FUNCIONES UTILIZADAS E IMPLEMENTADAS DE SEGMENTACION
 */
// funciones de segmentacion
void detectHScolor(const cv::Mat& image,		// imagen de entrada
                   double minHue, double maxHue,	// intervalo HUE
                   double minSat, double maxSat,	// intervalo saturacion
                   cv::Mat& mask) {				// mascara de salida

    cv::Mat hsv;
    cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);

    // split the 3 channels into 3 images
    std::vector<cv::Mat> channels;
    cv::split(hsv, channels); //channels[0] Hue, channels[1] Saturation, channels[2] Value

    //HUE MASK
    cv::Mat mask1; // bajo maxHue
    cv::threshold(channels[0], mask1, maxHue, 255, cv::THRESH_BINARY_INV);
    cv::Mat mask2; // sobre minHue
    cv::threshold(channels[0], mask2, minHue, 255, cv::THRESH_BINARY);

    cv::Mat hueMask;
    if (minHue < maxHue)
        hueMask = mask1 & mask2;
    else // si e intervalo cruza el 0 grado axis
        hueMask = mask1 | mask2;

    // SATURATION MASK
    // bajo maxsat
    cv::threshold(channels[1], mask1, maxSat, 255, cv::THRESH_BINARY_INV);
    // sobre minsat
    cv::threshold(channels[1], mask2, minSat, 255, cv::THRESH_BINARY);

    cv::Mat satMask;
    satMask = mask1 & mask2;

    // MASK COMBINADAS
    mask = hueMask&satMask;
}

int main(int argc, char *argv[]) {

    /*
     * WRITE YOUR CODE HERE!!
     * obtener: imagen de segmentacion de las manzanas, con cada region de manzana no conectada con otra
     */

    /* ETAPA 1: CONTROL DE SEGMENTACION A TRAVES DEL COLOR */
    //mi objetivo en esta seccion es realizar una mascara de colo para luego recuperar las
    //manzanas segmentadas, eso ayuda a visualizar que sectores de la imagen se estan perdiendo o
    //sobredimensionando.

    cv::Mat image= cv::imread("apples.png");
    if (!image.data)
        return 0;

    // show original image
    cv::namedWindow("Original image");
    cv::imshow("Original image",image);

    // detectar el verde en grados
    cv::Mat mask_color;
    detectHScolor(image,
                  10, 150, // hue from 320 degrees to 20 degrees --- 20, 70
                  70, 255, // saturation from ~0.1 to 0.65 --------- 70, 255
                  mask_color);

    // show masked image
    cv::Mat detected(image.size(), CV_8UC3, cv::Scalar(0, 0, 0));
    image.copyTo(detected, mask_color);
    cv::imshow("Detection result",detected);
    cv::imwrite("result.png", detected);

    detectHScolor(detected,
                  0, 50, // hue from 320 degrees to 20 degrees --- 20, 70
                  0, 255, // saturation from ~0.1 to 0.65 --------- 70, 255
                  mask_color);

    cv::Mat detected2(image.size(), CV_8UC3, cv::Scalar(0, 0, 0));
    image.copyTo(detected2, mask_color);
    cv::imshow("Detection result2",detected2);
    cv::imwrite("result2.png", detected2);

    /* ETAPA 2; CREACION DE LA MASCARA */
    //leemos la imagen en ByN, para tener un mapa del color segmentado (predominante casi solo verde)
    //nuestro objetivo es umbralizar lo que es verde con el resto de componentes poco verdes
    // en escala blanco y negro esto es facil observando la imagen provista por opencv
    cv::Mat result = cv::imread("result.png",0);
    cv::namedWindow("OriginalImage");
    cv::imshow("OriginalImage",result);

    int nr = result.rows;
    int nc = result.cols * result.channels();

    //optimizador si la imagen es continua
    if (result.isContinuous()){
        nc = nc*nr;
        nr = 1;
    }
    // catalogo: encontrar segmentacion correcta de blanco y negro
    //esto es poco  riguroso como umbral
    //simplemente le di en el gusto al medidor de IoU para obtener el mayor
    // perillando
    for (int j=0; j<nr; j++){
        uchar* data = result.ptr<uchar>(j);
        for (int i=0; i<nc; i++){
            if (data[i] > 75) data[i] = 255; //mirando la ventana de opencv de la imagen es facil
            else data[i] = 0;               //65-78 es bueno
        }
    }

    cv::namedWindow("etapa2");
    cv::imshow("etapa2",result);

    int esize = 2;
    cv::erode(result, result, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));
    cv::dilate(result, result, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));
    cv::dilate(result, result, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));
    cv::erode(result, result, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize, esize)));

    cv::namedWindow("final");
    cv::imshow("final",result);

    /* ETAPA 3: EVALUACION */
    //Evaluate the result against the groundtruth mask:
    cv::Mat mask = cv::imread("apples_mask.png");
    //evaluateResult(mask, result);

    cv::waitKey(0);

    return 0;
}
