#include<iostream>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <map>

using namespace std;

//Returns max dimension box
int paintRectangles(cv::Mat &img, std::map<int, cv::Rect> &bboxes) {
    if(bboxes.size() == 0)
        return -1;

    std::map<int, cv::Rect>::iterator it, it_end = bboxes.end();
    int max = 0, max_ind;
    for(it = bboxes.begin(); it != it_end; it++) {
        cv::Rect &r = it->second;
        if(r.width*r.height > max) {
            max = r.width*r.height;
            max_ind = it->first;
        }
    }

    for(it = bboxes.begin(); it != it_end; it++) {
        cv::Rect &r = it->second;
        if(it->first == max_ind)
            cv::rectangle(img, r, cv::Scalar(0,255,255), 2);
        else
            cv::rectangle(img, r, cv::Scalar(0,0,255), 2);
    }
    return max_ind;
}

void getBlobs(cv::Mat labels, std::map<int, cv::Rect> &bboxes) {
    int r = labels.rows, c = labels.cols;
    int label, x, y;
    bboxes.clear();
    for(int j=0; j<r; ++j)
        for(int i=0; i<c; ++i) {
            label = labels.at<int>(j,i);
            if(label > 0) {
                if(bboxes.count(label) == 0) { //New label
                    cv::Rect r(i,j,1,1);
                    bboxes[label] = r;
                } else { //Update rect
                    cv::Rect &r = bboxes[label];
                    x = r.x + r.width  - 1;
                    y = r.y + r.height - 1;
                    if(i < r.x) r.x = i;
                    if(i > x) x = i;
                    if(j < r.y) r.y = j;
                    if(j > y) y = j;
                    r.width = x - r.x + 1;
                    r.height = y - r.y + 1;
                }
            }
        }
}


int main(int argc, char *argv[]) {

    cv::VideoCapture vid;
    vid.open(4);
//    vid.open("TEST01.mp4");
//    cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7,7)));

    if(!vid.isOpened()) {
        cerr << "Error opening input." << endl;
        return 1;
    }

    cv::Mat img, bg, fg, labels;
    int historyMOG = 200; //Length of the history
    int nmixtures = 5; //Number of Gaussians Mixtures
    double backgroundRatio = 0.5; //Threshold of data that should be accounted for by the background (Parámetro T)
    double learningRate = 0.01; //Learning Rate
    double varThreshold = 100;


    bool bShadowDetection = false;
    cv::Ptr<cv::BackgroundSubtractorMOG2> mog = cv::createBackgroundSubtractorMOG2(historyMOG, varThreshold, bShadowDetection);
    mog->setNMixtures(nmixtures);
    mog->setBackgroundRatio(backgroundRatio);

/*
    mog->setVarThresholdGen(varThresholdGen);
    mog->setVarInit(fVarInit);
    mog->setVarMin(fVarMin);
    mog->setVarMax(fVarMax);
    mog->setComplexityReductionThreshold(fCT);
    mog->setDetectShadows(nShadowDetection);
    mog->setShadowThreshold(fTau);
    */


    bool first = true;
    int i=0,c;
    std::map<int, cv::Rect> bboxes;
    int biggest_bbox = -1;

    //1. Get a good initial window
    while(1) {
        vid >> img;
        i++;
        if(i>10) {
            if(first) {
                first = false;
                img.copyTo(bg);
                mog->apply(bg, fg, learningRate);
            } else {
                if(i<20) //Reinforce with initial background
                    mog->apply(bg, fg, learningRate);
                else
                    mog->apply(img, fg, learningRate);
                cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(11,11)));
                cv::dilate(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(11,11)));

                cv::dilate(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(11,11)));
                cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(11,11)));


                cv::connectedComponents(fg, labels, 8, CV_32S);
                bboxes.clear();
                getBlobs(labels, bboxes);
                biggest_bbox = paintRectangles(img, bboxes);

                imshow("Video", img);
                //imshow("Background", bg);
                imshow("Foreground", fg);


            }
        }
        if( (c=cv::waitKey(10)) != -1 && c == '1')
            break;
    }


    //2. Use camshift to track
    if(biggest_bbox >= 0) { //Big box available
        cv::Rect previous_bbox = bboxes[biggest_bbox];
        cv::Mat hsv, mask, hue, hist, backproj, histimg = cv::Mat::zeros(200, 320, CV_8UC3);
        int hsize = 16;
        float hranges[] = {0,180};
        const float* phranges = hranges;

        hsv.create(img.size(), img.type());

        cv::cvtColor(img, hsv, cv::COLOR_BGR2HSV);

        cv::inRange(hsv, cv::Scalar(0, 100, 50),
                    cv::Scalar(180, 255, 200), mask);

        mask &= fg;

        int ch[] = {0, 0};
        hue.create(hsv.size(), hsv.depth());
        //From hsv (1 image) to hue (1 image) using channel 0 of hsv, to channel 0 of hue (1 pair).
        cv::mixChannels(&hsv, 1, &hue, 1, ch, 1);

        //2.1 Set first histogram
        cv::Mat roi(hue, previous_bbox), maskroi(mask, previous_bbox);
        cv::calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
        cv::normalize(hist, hist, 0, 255, cv::NORM_MINMAX);

        histimg = cv::Scalar::all(0);
        int binW = histimg.cols / hsize;
        cv::Mat buf(1, hsize, CV_8UC3);
        for( int i = 0; i < hsize; i++)
            buf.at<cv::Vec3b>(i) = cv::Vec3b(cv::saturate_cast<uchar>(i*180./hsize), 255, 255);
        cv::cvtColor(buf, buf, cv::COLOR_HSV2BGR);

        for( int i = 0; i < hsize; i++) {
            int val = cv::saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
            cv::rectangle( histimg, cv::Point(i*binW,histimg.rows),
                           cv::Point((i+1)*binW,histimg.rows - val),
                           cv::Scalar(buf.at<cv::Vec3b>(i)), -1, 8 );
        }

        cv::imshow("Histograma", histimg);

        // Perform CAMShift
        cv::calcBackProject(&hue, 1, 0, hist, backproj, &phranges);

        backproj &= mask;
        cv::RotatedRect trackBox = cv::CamShift(backproj, previous_bbox,
                                cv::TermCriteria( cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1 ));


        cv::ellipse(img, trackBox, cv::Scalar(0,0,255), 3, cv::LINE_AA);

        cv::imshow("Video", img);

        int i=0;
        while(1) {
            vid >> img;
            cv::cvtColor(img, hsv, cv::COLOR_BGR2HSV);
            cv::mixChannels(&hsv, 1, &hue, 1, ch, 1);
            cv::calcBackProject(&hue, 1, 0, hist, backproj, &phranges);

            cv::inRange(hsv, cv::Scalar(0, 50, 50),
                        cv::Scalar(180, 256, 200), mask);

            backproj &= mask;
            cv::RotatedRect trackBox = cv::CamShift(backproj, previous_bbox,
                                    cv::TermCriteria( cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1 ));
            previous_bbox = trackBox.boundingRect();

            cv::ellipse(img, trackBox, cv::Scalar(0,0,255), 3, cv::LINE_AA);

            cv::imshow("Video", img);
            c = cv::waitKey(30);

            if(c != -1)
                break;

        }

    }

    vid.release();

    return 0;
}



