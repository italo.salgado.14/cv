cmake_minimum_required(VERSION 3.19)
project(OPENCV_FELZENSZWALB_CUDA)

set(CMAKE_CXX_STANDARD 11)
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_NO_BOOST_CMAKE ON)

include_directories(.)
FIND_PACKAGE( OpenCV REQUIRED )
find_package(Boost COMPONENTS fiber system filesystem REQUIRED)
add_executable(OPENCV_FELZENSZWALB_CUDA
        main.cpp)
TARGET_LINK_LIBRARIES( OPENCV_FELZENSZWALB_CUDA ${OpenCV_LIBS} Boost::headers Boost::fiber Boost::system Boost::filesystem)