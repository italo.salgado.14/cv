#include <string>
#include <vector>
#include<fstream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//Global fundamental matrix, for callback
/*
 */

cv::Mat F;
cv::Mat lout, rout;
cv::Mat limg, rimg;

void drawLeft(cv::Mat &Lr, int x, int y) {
    int rad = 5;
    limg.copyTo(lout);
    cv::circle(lout, cv::Point(x,y),rad,cv::Scalar(0,255,0));
    cv::imshow("Left Image", lout);

    rimg.copyTo(rout);
    double a = Lr.at<double>(0,0), b = Lr.at<double>(1,0), c = Lr.at<double>(2,0);
    if(a == 0 && b == 0)
        return;
    if(a == 0)
        cv::line(rout, cv::Point(0,-c/b), cv::Point(rout.cols-1,-c/b), cv::Scalar(0,255,0),3);
    else if(b == 0)
        cv::line(rout, cv::Point(-c/a, 0), cv::Point(-c/a, rout.rows-1), cv::Scalar(0,255,0),3);
    else
        cv::line(rout, cv::Point(0, -c/b), cv::Point(rout.cols-1, -a*(rout.cols-1)/b -c/b), cv::Scalar(0,255,0),3);

    cv::imshow("Right Image", rout);

}

void drawRight(cv::Mat &Ll, int x, int y) {
    int rad = 5;
    rimg.copyTo(rout);
    cv::circle(rout, cv::Point(x,y),rad,cv::Scalar(0,255,0));
    cv::imshow("Right Image", rout);

    limg.copyTo(lout);
    double a = Ll.at<double>(0,0), b = Ll.at<double>(0,1), c = Ll.at<double>(0,2);
    if(a == 0 && b == 0)
        return;
    if(a == 0)
        cv::line(lout, cv::Point(0,-c/b), cv::Point(lout.cols-1,-c/b), cv::Scalar(0,255,0),3);
    else if(b == 0)
        cv::line(lout, cv::Point(-c/a, 0), cv::Point(-c/a, lout.rows-1), cv::Scalar(0,255,0),3);
    else
        cv::line(lout, cv::Point(0, -c/b), cv::Point(lout.cols-1, -a*(lout.cols-1)/b -c/b), cv::Scalar(0,255,0),3);

    cv::imshow("Left Image", lout);

}



void LeftCallBack(int event, int x, int y, int flags, void* userdata) {
    if  ( event == cv::EVENT_LBUTTONDOWN ) {
        std::cout << "Left Image: Left button of the mouse is clicked - position (" << x << ", " << y << ")" << std::endl;
        cv::Mat ql = cv::Mat::ones(3, 1, CV_64FC1);
        ql.at<double>(0,0) = x;
        ql.at<double>(1,0) = y;
        std::cout << ql << std::endl;
        cv::Mat Lr = F*ql;
        std::cout << Lr << std::endl;
        drawLeft(Lr, x, y);
    }
}

void RightCallBack(int event, int x, int y, int flags, void* userdata) {
    if  ( event == cv::EVENT_LBUTTONDOWN ) {
        std::cout << "Right Image: Left button of the mouse is clicked - position (" << x << ", " << y << ")" << std::endl;
        cv::Mat qr = cv::Mat::ones(3, 1, CV_64FC1);
        qr.at<double>(0,0) = x;
        qr.at<double>(1,0) = y;
        std::cout << qr << std::endl;
        cv::Mat Ll = qr.t()*F;
        std::cout << Ll << std::endl;
        drawRight(Ll, x, y);
    }
}



// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

static int convert_to_int(std::string s) {
    int x = 0;
    std::stringstream convert(s);//object from the class stringstream.
    convert >> x;
    return x;
}

int main(int argc, char *argv[]) {
    std::string left = "left.png";
    std::string right = "right.png";
    std::string config = "puntos-conos.txt";

    std::vector<cv::Point2i> left_points, right_points;
    std::ifstream read;

    read.open(config);
    if(!read.is_open()) {
        std::cout << "Error opening '" << config << "'.\n";
        return 1;
    }

    std::string sline;
    char line[1000];
    int i = 0, aux;
    std::string s_xl, s_yl, s_xr, s_yr;
    int xl, yl, xr, yr;

    while(!read.eof()){
        read.getline(line, 1000);
        sline = line;
        sline = trim(sline);
        if(sline.empty() || sline[0] == '#')
            continue;
        i++;
        std::cout << i << ": '" << sline << "'" << std::endl;
        aux = sline.find("\t");
        s_xl = sline.substr(0, aux);
        sline = sline.substr(aux+1);
        aux = sline.find("\t");
        s_yl = sline.substr(0, aux);
        sline = sline.substr(aux+1);
        aux = sline.find("\t");
        s_xr = sline.substr(0, aux);
        s_yr = sline.substr(aux+1);
        xl = convert_to_int(s_xl);
        yl = convert_to_int(s_yl);
        xr = convert_to_int(s_xr);
        yr = convert_to_int(s_yr);

        std::cout << "\t(" << xl << "," << yl << ") --> (";
        std::cout << xr << "," << yr << ")" << std::endl;

        left_points.push_back(cv::Point2i(xl,yl));
        right_points.push_back(cv::Point2i(xr,yr));

    }

    if(left_points.size() != right_points.size()) {
        std::cout << "Error reading '" << config << "'. Left and right point list shall be of the same size.\n";
        return 1;
    }

    if(left_points.size() < 8) {
        std::cout << "Error! There should be at least 8 correspondences, and only found: " << left_points.size() << std::endl;
        return 1;
    }

    F = cv::findFundamentalMat(left_points, right_points);
    std::cout << "F = " << F << std::endl;

    limg = cv::imread(left);
    rimg = cv::imread(right);
    if(limg.empty()) {
        std::cout << "Error reading left image '" << left << "'." << std::endl;
        return 1;
    }
    if(rimg.empty()) {
        std::cout << "Error reading right image '" << right << "'." << std::endl;
        return 1;
    }

    //Create windows
    cv::namedWindow("Left Image", 1);
    cv::namedWindow("Right Image", 1);

    //Set the callback functions for left mouse event
    cv::setMouseCallback("Left Image", LeftCallBack, NULL);
    cv::setMouseCallback("Right Image", RightCallBack, NULL);

    //show the image
    cv::imshow("Left Image", limg);
    cv::imshow("Right Image", rimg);

    //Wait for user hitting key
    cv::waitKey(0);

    //Uncalibrated rectification
    cv::Mat Hl, Hr;

    //algoritmo de HARTLEY
    cv::stereoRectifyUncalibrated(left_points, right_points, F, limg.size(), Hl, Hr);

    std::cout << "Hl = " << Hl << std::endl;
    std::cout << "Hr = " << Hr << std::endl;

    cv::Mat rectified1(limg.size(), limg.type());
    //ver la perspectiva luego de aplicar la homografia
    cv::warpPerspective(limg, rectified1, Hl, limg.size());
    cv::imshow("Rectified Left Image", rectified1);

    cv::Mat rectified2(rimg.size(), rimg.type());
    //tomar todos los puntos de la imagen original y proyectarlo con la homografia al plano correspondiente a esa
    //transformacion. Para llegar a un arreglo frontal paralelo
    cv::warpPerspective(rimg, rectified2, Hr, rimg.size());
    cv::imshow("Rectified Right Image", rectified2);

    //Wait for user hitting key
    cv::waitKey(0);

    cv::Mat leftgray(limg.rows, limg.cols, CV_8UC1),
            rightgray(rimg.rows, rimg.cols, CV_8UC1);

    cv::cvtColor(limg, leftgray, cv::COLOR_BGR2GRAY);
    cv::cvtColor(rimg, rightgray, cv::COLOR_BGR2GRAY);

    int ndisparities = 16*5;   /**< Range of disparity */
    int SADWindowSize = 21; /**< Size of the block window. Must be odd */

    cv::Ptr<cv::StereoBM> sbm = cv::StereoBM::create( ndisparities, SADWindowSize );

    cv::Mat imgDisparity16S = cv::Mat(limg.rows, limg.cols, CV_16SC1);
    cv::Mat imgDisparity8U = cv::Mat(limg.rows, limg.cols, CV_8UC1);

    sbm->compute(leftgray, rightgray, imgDisparity16S);

    double minVal, maxVal;

    cv::minMaxLoc(imgDisparity16S, &minVal, &maxVal);
    imgDisparity16S.convertTo(imgDisparity8U, CV_8UC1, 255/(maxVal - minVal));

    //cv::imshow("Disparity", imgDisparity8U);
    cv::Mat im_color;
    cv::applyColorMap(imgDisparity8U, im_color, cv::COLORMAP_JET);
    cv::imshow("Disparity Heat", im_color);

    cv::Ptr<cv::StereoSGBM> sgbm = cv::StereoSGBM::create(0, ndisparities, 7);

    cv::Mat imgDisparity16S_2 = cv::Mat(limg.rows, limg.cols, CV_16SC1);
    cv::Mat imgDisparity8U_2 = cv::Mat(limg.rows, limg.cols, CV_8UC1);

    sgbm->compute(leftgray, rightgray, imgDisparity16S_2);
    cv::minMaxLoc(imgDisparity16S_2, &minVal, &maxVal);
    imgDisparity16S_2.convertTo(imgDisparity8U_2, CV_8UC1, 255/(maxVal - minVal));

    //cv::imshow("Disparity", imgDisparity8U);
    cv::Mat im_color_2;
    cv::applyColorMap(imgDisparity8U_2, im_color_2, cv::COLORMAP_JET);
    cv::imshow("Disparity Heat 2", im_color_2);


    //Wait for user hitting key
    cv::waitKey(0);

    return 0;
}
