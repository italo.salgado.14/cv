#include<iostream>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

using namespace std;
using namespace cv;



int main(int argc, char *argv[]) {

    cv::VideoCapture vid;

    vid.open(0);

    if(!vid.isOpened()) {
        cerr << "Error opening input." << endl;
        return 1;
    }

    /*
     * Necesita definir un patron de chessboard:
     * el chessboard tiene 12x10 esquinas verticales x horizontales
     */

    cv::Mat img, img_cpy, gray;
    int c;
    std::vector<Point3f> obj;
    int board_w = 12; //12 horizontales
    int board_h = 10; //10 verticales
    int board_n = board_w * board_h;

    //representa el numero de esquinas verticales y horizontales
    //que es lo que tiene que buscar la funcion
    cv::Size patternsize = cv::Size(board_w, board_h);
    for(int j=0;j<board_n;j++)
        //calibración con una medida arbitraria en metros, con altura z = 0
        //inicializar los puntos de objeto
        obj.push_back(Point3f(j/board_w, j%board_w, 0.0f));

    TermCriteria criteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.1);
    int numPerspectiveFrames = 0;
    int neededPerspectiveFrames = 40;
    int currentPerspectiveFrames = 0;
    int totalPerspectiveFrames = 10;

    std::vector<std::vector<Point3f> > object_points;
    std::vector<std::vector<Point2f> > image_points;
    bool perspectiveOK = false;
    cv::Mat intrinsic_matrix(3,3,CV_64FC1);
    //para la funcion de distosion le pasamos 8 parametros (1 columna), prisma delgado fuera
    //solo radial y tangencia (8), los ultimos 4 de prisma delgado no estan incluidos
    // si rows=12 incluiria los de prisma delgado
    cv::Mat distortion_coeffs = cv::Mat::zeros(8,1,CV_64FC1);
    std::vector<Point2f> corners;

    while(true) {
        vid >> img;
        cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

        img.copyTo(img_cpy);

        if(!perspectiveOK) {
            numPerspectiveFrames++;
            if(numPerspectiveFrames == neededPerspectiveFrames) {
                numPerspectiveFrames = 0;

                //detectar los bordes, guardando las esquinas en corners
                if(cv::findChessboardCorners(gray, patternsize, corners, cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FILTER_QUADS)) {
                    currentPerspectiveFrames++;
                    std::cout << "Found corners " << currentPerspectiveFrames << "times!" << std::endl;

                    cv::cornerSubPix(gray, corners, Size(11, 11), Size(-1, -1), criteria);
                    cv::drawChessboardCorners(img_cpy, patternsize, cv::Mat(corners), true);

                    image_points.push_back(corners); //le agregamos acá las esquinas detectadas
                    object_points.push_back(obj); //le agregamos aca los puntos de objeto

                    cv::imshow("Detected chessboard", img_cpy);

                    if(currentPerspectiveFrames == totalPerspectiveFrames) {
                        perspectiveOK = true;
                        //la matriz intrínseca
                        intrinsic_matrix.ptr<float>(0)[0] = 1;
                        intrinsic_matrix.ptr<float>(1)[1] = 1;
                        //CALIBRATE THE CAMERA!
                        std::vector<Mat> rvecs;
                        std::vector<Mat> tvecs;

                        cv::calibrateCamera(object_points, image_points,
                            img.size(),
                            //intrinsic_matrix, distortion_coeffs, rvecs, tvecs,  cv::CALIB_RATIONAL_MODEL | cv::CALIB_ZERO_TANGENT_DIST | cv::CALIB_FIX_K4 | cv::CALIB_FIX_K5 | cv::CALIB_FIX_K6); //cv::CALIB_FIX_ASPECT_RATIO
                            intrinsic_matrix, distortion_coeffs, rvecs, tvecs,  cv::CALIB_RATIONAL_MODEL | cv::CALIB_THIN_PRISM_MODEL); //cv::CALIB_FIX_ASPECT_RATIO

                            std::cout << "Intrinsic:" << intrinsic_matrix << std::endl;
                            std::cout << "Distorsion:" << distortion_coeffs << std::endl;

                            //Draw something nice
                            cv::Mat imageUndistorted;
                            //aplicar la funcion de transformacion, la aplicacion de la funcion desdistorsionar es
                            //independiente de los parametros extrisecos, solo con los parametros instrinsecos
                            //le pasamos la matriz intrinseca, los coeficientes de distorsion.
                            cv::undistort(img_cpy, imageUndistorted, intrinsic_matrix, distortion_coeffs);
                            cv::imshow("Current Frame Undistorted", imageUndistorted);

                    }
                } else if(currentPerspectiveFrames > 0) { //Show last corners
                        cv::drawChessboardCorners(img_cpy, patternsize, cv::Mat(corners), true);
                }
            }
            cv::imshow("Current Frame", img_cpy);

        } else {
            cv::drawChessboardCorners(img_cpy, patternsize, cv::Mat(corners), true);
            cv::imshow("Current Frame", img_cpy);
        }

        c=cv::waitKey(10);
        if(c != -1)
            break;
    }

    vid.release();





    return 0;
}



cv::Mat sobelGradient(cv::Mat src) {
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

    /// Generate grad_x and grad_y
    cv::Mat grad, grad_x, grad_y, abs_grad_x, abs_grad_y;

    /// Gradiente X
    cv::Sobel(src, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT);
    cv::convertScaleAbs(grad_x, abs_grad_x);

    /// Gradiente Y
    cv::Sobel(src, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);

    /// Total Gradient (approximate)
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

    double maxVal;
    cv::minMaxLoc(grad, NULL, &maxVal, NULL, NULL);
    double factor = 255/maxVal;

    cv::Mat m8u;

    grad.convertTo(m8u, CV_8U, factor);


    return m8u;
}
