#include<iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ximgproc/seeds.hpp>
#include <opencv2/ximgproc/slic.hpp>


using namespace std;

//mascara de etiqueta indican si pertenece a un cluster u a otro
void paintMask(cv::Mat im, cv::Mat mask) {
    int i, j, pixels = im.rows*im.cols;
    uchar *im_data = im.data, *m_data = mask.data;
    for(i=0, j=0; i<pixels; ++i, j+=3)
        if(m_data[i] == 255) {
            im_data[j] = 0;
            im_data[j+1] = 0;
            im_data[j+2] = 255;
        }
    }
int main(int argc, char *argv[]) {

    if(argc == 1) {
        cerr << "Usage: ./OPENCV_SUPERPIXELS image" << endl;
        return 1;
    }

    if(argc == 1) {
        cerr << "Usage: ./OPENCV_SUPERPIXELS image" << endl;
        return 1;
    }

    cv::Mat im = cv::imread( argv[1], 1 );
    cv::imshow("Entrada", im);

    //Parameter set for module not defined
    //Desired number of superpixels. Note that the actual number may be smaller due to restrictions (depending on the image size and num_levels).
    //Use getNumberOfSuperpixels() to get the actual number.
    int m_superpixelnumber = 120;

    //Number of block levels. The more levels, the more accurate is the segmentation, but needs more memory and CPU time.
    int m_num_levels = 5;

    //Use prior – enable 3x3 shape smoothing term if >0. A larger value leads to smoother shapes. prior must be in the range [0, 5].
    int m_use_prior = 4;

    //Number of histogram bins.
    int m_histogram_bins = 10;

    //If true, iterate each block level twice for higher accuracy. Iterar mas de una vez?
    int m_double_step = true;

    //Number of pixel level iterations. Higher number improves the result.
    int m_num_iterations = 10000;

    //false = SLIC, SLICO o MSLIC. true = SEEDS (mas rapido)
    bool SEEDS = false;
    if(SEEDS) {
        //un puntero compartido. Parametros:
        // num_levels: que niveles de grupos mas grandes consideraremos
        cv::Ptr<cv::ximgproc::SuperpixelSEEDS> m_SEEDS =
                cv::ximgproc::createSuperpixelSEEDS(im.cols, im.rows, 3, m_superpixelnumber,
                                                    m_num_levels, m_use_prior,
                                                    m_histogram_bins, m_double_step);

        m_SEEDS->iterate(im, m_num_iterations);

        cv::Mat results(im.size(), CV_32SC1);

        m_SEEDS->getLabelContourMask(results, true);
//    m_SEEDS->getLabels(results);

        paintMask(im, results);
        cv::imshow("Resultados", im);

    } else {
        //Choose the algorithm:
        //  cv::ximgproc::SLIC
        //  cv::ximgproc::SLICO - no le pazamos parametros
        //  cv::ximgproc::MSLIC
        int algorithm = cv::ximgproc::SLICO;

        //Chooses an average superpixel size measured in pixels, lo que esperariamos
        int region_size = 10;

        //Chooses the enforcement of superpixel compactness factor of superpixel
        float ratio	= 0.1;

        cv::Ptr<cv::ximgproc::SuperpixelSLIC> m_SLIC =
                cv::ximgproc::createSuperpixelSLIC(im,  algorithm, region_size, ratio);
        m_num_iterations = 1000;
        m_SLIC->iterate(m_num_iterations);

        cv::Mat results(im.size(), CV_32SC1);

        m_SLIC->getLabelContourMask(results, true);
//    m_SEEDS->getLabels(results);

        paintMask(im, results);
        cv::imshow("Resultados", im);

    }
    cv::waitKey(0);



    return 0;
}
