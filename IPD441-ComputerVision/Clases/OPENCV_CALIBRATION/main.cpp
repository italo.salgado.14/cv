#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <map>

using namespace std;

cv::Point2i transform(cv::Point2f p, cv::Mat &H) {
    cv::Mat pin(3, 1, CV_64FC1);
    pin.at<double>(0,0) = p.x;
    pin.at<double>(1,0) = p.y;
    pin.at<double>(2,0) = 1;

    cv::Mat pout = H*pin;

    return cv::Point2i(rint(pout.at<double>(0,0)/pout.at<double>(2,0)),
                       rint(pout.at<double>(1,0)/pout.at<double>(2,0)));
}

void drawRectangle(cv::Mat &img, cv::Point2f &p, cv::Mat &H) {
    int hside = 50;
    cv::line(img, transform(cv::Point2f(p.x-hside, p.y-hside), H),
             transform(cv::Point2f(p.x+hside, p.y-hside), H), cv::Scalar(0,255,255));
    cv::line(img, transform(cv::Point2f(p.x+hside, p.y-hside), H),
             transform(cv::Point2f(p.x+hside, p.y+hside), H), cv::Scalar(0,255,255));
    cv::line(img, transform(cv::Point2f(p.x+hside, p.y+hside), H),
             transform(cv::Point2f(p.x-hside, p.y+hside), H), cv::Scalar(0,255,255));
    cv::line(img, transform(cv::Point2f(p.x-hside, p.y+hside), H),
             transform(cv::Point2f(p.x-hside, p.y-hside), H), cv::Scalar(0,255,255));
}

void addPoint(cv::Mat &img, cv::Point2f &p, cv::Mat &H, int index) {
    //Get center in image coordinates
    cv::Point2i p_im = transform(p, H);

    //Draw circle - pintar circulos, cuidado con el color
    cv::circle(img, p_im, 3, cv::Scalar(0,255,255));

    //Draw rectangle
    drawRectangle(img, p, H);

    //Draw text
    cv::putText(img, std::to_string(index),
                cv::Point(p_im.x+2,p_im.y), cv::FONT_HERSHEY_DUPLEX, 0.5,
                cv::Scalar(0,255,0));


}

void getBlobs(cv::Mat labels, std::map<int, cv::Rect> &bboxes) {
    int r = labels.rows, c = labels.cols;
    int label, x, y;
    bboxes.clear();
    for(int j=0; j<r; ++j)
        for(int i=0; i<c; ++i) {
            label = labels.at<int>(j,i);
            if(label > 0) {
                if(bboxes.count(label) == 0) { //New label
                    cv::Rect r(i,j,1,1);
                    bboxes[label] = r;
                } else { //Update rect
                    cv::Rect &r = bboxes[label];
                    x = r.x + r.width  - 1;
                    y = r.y + r.height - 1;
                    if(i < r.x) r.x = i;
                    if(i > x) x = i;
                    if(j < r.y) r.y = j;
                    if(j > y) y = j;
                    r.width = x - r.x + 1;
                    r.height = y - r.y + 1;
                }
            }
        }
}

void paintRectangle(cv::Mat &img, cv::Rect &bbox) {
    cv::rectangle(img, bbox, cv::Scalar(0,0,255), 2);
}



int main(int argc, char *argv[]) {

    if(argc != 3) {
        cerr << "Usage: ./OPENCV_CALIBRATION bg sample" << endl;
        return 1;
    }

    cv::Mat img = cv::imread( argv[1], 1 );

    if(img.empty()) {
        cerr << "Error reading image " << argv[1] << endl;
        return 1;
    }

    cv::Mat sample = cv::imread( argv[2], 1 );
    if(sample.empty()) {
        cerr << "Error reading image " << argv[2] << endl;
        return 1;
    }

    float scale = 2.5;

    cv::resize(img, img, cv::Size(scale*img.cols, scale*img.rows));
    cv::resize(sample, sample, cv::Size(scale*sample.cols, scale*sample.rows));

    cv::imshow("Background", img);
    cv::imshow("Sample", sample);


    //The nine scene points
    /* los puntos que yo conozco de la escena */
    /* estoy a dos metros entre cada uno de los centros de los rectangulos, en cm*/
    std::map<int, cv::Point2f> scenePoints;
    cv::Point2f p;
    p.x = -200; p.y = -200;
    scenePoints[1] = p;
    p.x = -200; p.y = 0;
    scenePoints[2] = p;
    p.x = -200; p.y = 200;
    scenePoints[3] = p;
    p.x = 0;     p.y = -200;
    scenePoints[4] = p;
    p.x = 0;     p.y = 0;
    scenePoints[5] = p;
    p.x = 0;     p.y = 200;
    scenePoints[6] = p;
    p.x = 200;  p.y = -200;
    scenePoints[7] = p;
    p.x = 200;  p.y = 0;
    scenePoints[8] = p;
    p.x = 200;  p.y = 200;
    scenePoints[9] = p;

    /* puntos de correspondencia en la imagen*/
    std::map<int, cv::Point2f> imagePoints;
    p.x = 112; p.y = 105;
    imagePoints[1] = p;
    p.x = 195; p.y = 100;
    imagePoints[2] = p;
    p.x = 279; p.y = 94;
    imagePoints[3] = p;
    p.x = 99;  p.y = 131;
    imagePoints[4] = p;
    p.x = 202; p.y = 123;
    imagePoints[5] = p;
    p.x = 300; p.y = 116;
    imagePoints[6] = p;
    p.x = 80;  p.y = 172;
    imagePoints[7] = p;
    p.x = 214; p.y = 161;
    imagePoints[8] = p;
    p.x = 344;  p.y = 152;
    imagePoints[9] = p;

    //Scale img points:
    for(int i=0; i<imagePoints.size(); ++i) {
        imagePoints[i].x *= scale;
        imagePoints[i].y *= scale;
    }

    //Select the points to calibrate: you can choose among the nine
    std::vector<cv::Point2f> sceneVector; //el vector de la escena. No estoy ocupando todo los puntos, solo los
    sceneVector.push_back(scenePoints[4]); //que estan adelante porque me dan mas confianza que tengan menos error
    sceneVector.push_back(scenePoints[5]);
    sceneVector.push_back(scenePoints[7]);
    sceneVector.push_back(scenePoints[8]);
    sceneVector.push_back(scenePoints[9]);
    std::vector<cv::Point2f> imageVector; //el vector de la imagen
    imageVector.push_back(imagePoints[4]);
    imageVector.push_back(imagePoints[5]);
    imageVector.push_back(imagePoints[7]);
    imageVector.push_back(imagePoints[8]);
    imageVector.push_back(imagePoints[9]);

    //aqui obtengo la homografía
    //obtendre la homografia que me permite ir de puntos de la escena a los puntos de la imagen
    cv::Mat H = cv::findHomography(sceneVector, imageVector); //vector de la escena y vector de la img

    cout << "H = " << H << endl;

    //Examples of points transformed with the homography:
    cv::Mat pin(3, 1, CV_64FC1);
    pin.at<double>(0,0) = 0;
    pin.at<double>(1,0) = 0;
    pin.at<double>(2,0) = 1;

    cout << "p5 = " << H*pin << endl; //le paso un punto de la escena y obtengo un punto de la imagen
    //podemos invertir H para obtener la tranbsofrmación correspondiente a la inversa

    pin.at<double>(0,0) = 200;
    pin.at<double>(1,0) = 200;
    pin.at<double>(2,0) = 1;

    cv::Mat pout = H*pin;
    cout << "p9 = " << pout/pout.at<double>(2,0) << endl;

    pin.at<double>(0,0) = -200;
    pin.at<double>(1,0) = -200;
    pin.at<double>(2,0) = 1;

    pout = H*pin;
    cout << "p1 = " << pout/pout.at<double>(2,0) << endl;

    cv::Mat out, out2;
    img.copyTo(out);
    sample.copyTo(out2);

    for(int i = 1; i<= 9; i++)
        /*
         * Le pasamos cada uno de los puntos de la escena, la matriz de homografía
         * y leugo pinta circulos en la imagen. los 9 puntos se utilizan, siendo que usamos 5 para
         * encontrar la matriz de homografia. Nuestra proyeccion es buena!
         * Usar mas puntos para construir H auemnta la persicion
         */
        addPoint(out, scenePoints[i], H, i);

    /*
     * etapa de deteccion de la persona aprovechandonos de lo calculado
     * podria utilizar un mapa y saber donde esta la persona en ese sector
     */
    //Segmentation
    cv::Ptr<cv::BackgroundSubtractorMOG2> mog;
    int historyMOG = 100; //Length of the history
    int nmixtures = 3; //Number of Gaussians Mixtures
    double backgroundRatio = 0.5; //Threshold of data that should be accounted for by the background (Parámetro T)
    double learningRate = 0.05; //Learning Rate
    double varThreshold = 60; //Umbral segmentacion


    bool bShadowDetection = true;
    mog = cv::createBackgroundSubtractorMOG2(historyMOG, varThreshold, bShadowDetection);
    mog->setNMixtures(nmixtures);
    mog->setBackgroundRatio(backgroundRatio);
/*
    mog->setVarThresholdGen(varThresholdGen);
    mog->setVarInit(fVarInit);
    mog->setVarMin(fVarMin);
    mog->setVarMax(fVarMax);
    mog->setComplexityReductionThreshold(fCT);
    mog->setDetectShadows(nShadowDetection);
    mog->setShadowThreshold(fTau);
    */

    int i=0;
    cv::Mat fg;

    for(i=0; i<100; ++i)
        mog->apply(img, fg, learningRate);

    mog->apply(sample, fg, learningRate);

    cv::imshow("Fg1", fg);

    cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5)));
    cv::dilate(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5)));

    cv::dilate(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5)));
    cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5)));
    cv::imshow("Fg2", fg);

    cv::threshold(fg, fg, 200, 255, 0);
    cv::imshow("Fg1.5", fg);


    cv::Mat labels;
    cv::connectedComponents(fg, labels, 8, CV_32S);
    std::map<int, cv::Rect> bboxes;
    getBlobs(labels, bboxes);

    int max = 0, max_i;

    if(bboxes.size() > 0) {
        for(i=0; i<bboxes.size(); ++i)
            if(bboxes[i].width * bboxes[i].height > max) {
                max = bboxes[i].width * bboxes[i].height;
                max_i = i;
            }
        paintRectangle(out2, bboxes[max_i]);
        cv::Point p(bboxes[max_i].x + bboxes[max_i].width/2, bboxes[max_i].y + bboxes[max_i].height);
        cv::circle(out2, p, 3, cv::Scalar(0,255,255),2);

        cv::Mat Hinv = H.inv();
        cv::Point2i pout = transform(p, Hinv);
        std::string s = "  (" + std::to_string(pout.x) + "; " + std::to_string(pout.y) + ")";
        cv::putText(out2, s, p, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0,255,255));

    }


    cv::imshow("Calibration", out);
    cv::imshow("Detection", out2);
    cv::waitKey();

    return 0;
}
