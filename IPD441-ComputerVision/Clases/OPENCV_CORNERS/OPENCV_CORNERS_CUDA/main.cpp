#include<iostream>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {

    cv::VideoCapture vid;

    vid.open(0);

    if(!vid.isOpened()) {
        cerr << "Error opening input." << endl;
        return 1;
    }

    cv::Mat img, img2;

    while(1) {
        vid >> img;
//        cv::imshow("Original", img);
        img.copyTo(img2);

        TermCriteria criteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.1);
        std::vector<Point2f> corners, corners2;

        cv::Mat gray(img.rows,img.cols,CV_8UC1);
        cv::cvtColor(img, gray,cv::COLOR_BGR2GRAY);

        //Harris
        //maxCorners es el parametro que controla la cantidad de puntos disponibles
        cv::goodFeaturesToTrack(gray, corners, 100, 0.01, 5, cv::noArray(), 3, true, 0.04);
        std::cout << "Found Harris corners: " << corners.size()  << std::endl;

        if(corners.size() > 0)
            //ajustar la posicion exacta del punto de interés en base a los cambios de intensidad de gradiente, resolviendo
            //un sistema lineal. Se calcula un punto corregido, podemos entonces entrear un punto óptimo-corregido donde
            // debería estar el gradiente (con mas precisión, y mas decimales). Corrección típica
            cv::cornerSubPix(gray, corners, Size(11, 11), Size(-1, -1), criteria);
        for( size_t i = 0; i < corners.size(); i++ ) {
            cv::circle(img, corners[i], 5, cv::Scalar( 255. ), -1 );
        }

        //GoodFeaturesToTrack
        cv::goodFeaturesToTrack(gray, corners2, 100, 0.01, 5, cv::noArray(), 3, false, 0.04);
        std::cout << "Found good features to track corners: " << corners.size()  << std::endl;
        if(corners2.size() > 0)
            cv::cornerSubPix(gray, corners2, Size(11, 11), Size(-1, -1), criteria);
        for( size_t i = 0; i < corners2.size(); i++ ) {
            cv::circle(img2, corners2[i], 5, cv::Scalar(0,0,255), -1 );
        }

        cv::imshow("Harris", img);
        cv::imshow("GoodFeaturesToTrack", img2);

        int c=cv::waitKey(10);
        //std::cout << c << std::endl;
        if(c != -1)
            break;
        corners.clear();
        corners2.clear();
    }

    vid.release();

    return 0;
}

