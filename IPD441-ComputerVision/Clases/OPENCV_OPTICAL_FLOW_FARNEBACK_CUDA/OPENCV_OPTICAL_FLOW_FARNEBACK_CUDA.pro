#-------------------------------------------------
#
# Project created by QtCreator 2016-08-08T15:03:08
#
#-------------------------------------------------

QT += core gui
QT += xml
#QT += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += APP_VERSION=\\\"1.3.2\\\"


#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TARGET = OPENCV_OPTICAL_FLOW_FARNEBACK
TEMPLATE = app

#CONFIG += link_pkgconfig

#LIBS += `pkg-config opencv --libs --cflags`

LIBS += `pkg-config \
    opencv \
    --cflags \
    --libs` \
    -Xlinker #\
#    -rpath /usr/local/lib
LIBS += -L/home/mzuniga/Qt/5.15.2/gcc_64/lib
LIBS += -L/usr/lib/x86_64-linux-gnu -lcudart -lcuda

INCLUDEPATH += /usr/local/include/opencv4

#include(/home/stefano/opencv-4.0.1/opencv.pri)

#PKGCONFIG += opencv

HEADERS  +=

FORMS    +=

DISTFILES +=

#Files to be compiled by g++
SOURCES += #main.cpp

#CUDA + OPENCV in QTCREATOR: Based on https://cudaspace.wordpress.com/2012/07/05/qt-creator-cuda-linux-review/

#QMAKE_CXXFLAGS += "-fno-sized-deallocation"

#Files to be compiled by NVCC
CUDA_SOURCES += main.cpp

#NVCCFLAGS     = --compiler-options -fno-strict-aliasing -use_fast_math --ptxas-options=-v
NVCCFLAGS = --compiler-options -use-fast-math --Wno-deprecated-gpu-targets

# Path to cuda toolkit install
CUDA_DIR      = /usr

INCLUDEPATH += $$CUDA_DIR/include

# GPU architecture
CUDA_ARCH     = sm_75                # Adjust with your compute capability

# Prepare the extra compiler configuration (taken from the nvidia forum)
CUDA_INC = $$join(INCLUDEPATH,' -I','-I',' ')

CONFIG(debug, debug|release) {
#cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -g -G arch=$$CUDA_ARCH -c $$NVCCFLAGS \
cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -g -G -c $$NVCCFLAGS \
                $$CUDA_INC $$LIBS ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
}
else {
cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -c $$NVCCFLAGS \
                $$CUDA_INC $$LIBS ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
}
#cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -arch=$$CUDA_ARCH -c $$NVCCFLAGS \
#cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -c $$NVCCFLAGS \
#                $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
#                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2

# nvcc error printout format ever so slightly different from gcc
# http://forums.nvidia.com/index.php?showtopic=171651

cuda.dependency_type = TYPE_C # there was a typo here. Thanks workmate!

CONFIG(debug, debug|release) {
    cuda.depend_command = $$CUDA_DIR/bin/nvcc -g -G -M $$CUDA_INC $$NVCCFLAGS   ${QMAKE_FILE_NAME}  | sed \"s/^.*: //\"
}
else {
    cuda.depend_command = $$CUDA_DIR/bin/nvcc -O3 -M $$CUDA_INC $$NVCCFLAGS   ${QMAKE_FILE_NAME}  | sed \"s/^.*: //\"
}

#cuda.depend_command = $$CUDA_DIR/bin/nvcc -O3 -M $$CUDA_INC $$LIBS $$NVCCFLAGS ${QMAKE_FILE_NAME}

cuda.input = CUDA_SOURCES
cuda.output = ${OBJECTS_DIR}${QMAKE_FILE_BASE}_cuda.o
# Tell Qt that we want add more stuff to the Makefile
QMAKE_EXTRA_COMPILERS += cuda

