#include<stdio.h>
#include<iostream>
#include"opencv2/core.hpp"
#include"opencv2/features2d.hpp"
#include"opencv2/xfeatures2d.hpp"
#include"opencv2/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d/cuda.hpp>

using namespace cv;
using namespace cv::xfeatures2d;
int c;

Mat collate(Mat img1, Mat img2) {
    Mat result(img1.rows, img1.cols*2, CV_8UC3);
    int i, im_step = img1.step,
           step = result.step,
           limit = step*result.rows;
    uchar *data = result.data, *d1 = img1.data, *d2 = img2.data;
    for(i=0; i<limit; i+=step) {
        memcpy(data, d1, im_step);
        memcpy(data + im_step, d2, im_step);
        d1 += im_step;
        d2 += im_step;
        data += step;
    }

    return result;
}

void detectKeypoints(Ptr<FeatureDetector> detector, cv::Mat &img,
                      std::vector<KeyPoint> &keypoints, cv::Mat &img_keypoints) {
    Mat gimg;
    //--Step 1: Transform to grayscale
    cvtColor(img, gimg, COLOR_BGR2GRAY);

    //--Step 2: Detect the keypoints using SIFT
    detector->detect(gimg, keypoints);

    //Draw keypoints
    drawKeypoints(img, keypoints, img_keypoints,
                  Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

}

void computeDescriptors(Ptr<FeatureDetector> detector, cv::Mat &img,
                        std::vector<KeyPoint> &keypoints,
                        Mat &descriptors,
                        cv::Mat &img_keypoints) {
    Mat gimg;
    //--Step 1: Transform to grayscale
    cvtColor(img, gimg, COLOR_BGR2GRAY);

    //--Step 2: Detect the keypoints
    detector->detectAndCompute(gimg, noArray(), keypoints, descriptors);

    //Draw keypoints
    drawKeypoints(img, keypoints, img_keypoints,
                  Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

}

void computeBinaryDescriptors(Ptr<FeatureDetector> detector, cv::Mat &img,
                        std::vector<KeyPoint> &keypoints,
                        Mat &descriptors,
                        cv::Mat &img_keypoints) {
    Mat gimg;
    //--Step 1: Transform to grayscale
    cvtColor(img, gimg, COLOR_BGR2GRAY);

    //--Step 2.1: Detect the keypoints
    detector->detect(gimg, keypoints);

    //--Step 2.2: Compute descriptors
    detector->compute(gimg, keypoints, descriptors);

    //Draw keypoints
    std::cout << "Num features: " << keypoints.size() << std::endl;
    drawKeypoints(img, keypoints, img_keypoints,
                  Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

}


void drawMatches(Mat &img, std::vector<DMatch> &matches,
                 std::vector<KeyPoint> &keypoints_1,
                 std::vector<KeyPoint> &keypoints_2) {

    int w2 = img.cols/2, m, msize = matches.size();
    Point2f p1, p2;
    for(int i; i<msize; ++i) {
        p1 = keypoints_1[matches[i].queryIdx].pt;
        p2 = keypoints_2[matches[i].trainIdx].pt;
        p2.x += w2;
        line(img, p1, p2, Scalar(255,0,0), 2);
    }
}



int main(int argc, char** argv){

    cv::VideoCapture vid;

    //aqui seteamos cual algoritmos debe ser implementado
    if(argc < 2) {
        std::cerr << "Usage: ./OPENCV_DESCRIPTORS descriptor" << std::endl;
        std::cerr << "\tdescriptor: \t1: SIFT." << std::endl;
        std::cerr << "\t \t2: SURF." << std::endl;
        std::cerr << "\t \t3: FREAK." << std::endl;
        return 1;
    }

    std::string desc = argv[1];
    Ptr<FeatureDetector> detector;
    bool binary = false;
    //--Step 0: Create Detector
    if (desc == "1") {
        //SIFT(int nfeatures=0, int nOctaveLayers=3, double contrastThreshold=0.04, double edgeThreshold=10, double sigma=1.6
        Ptr<SIFT> SIFTdetector = SIFT::create(100, 3, 0.04, 10, 1.6);
        detector = SIFTdetector;
    } else if (desc == "2") {
        //--Step 0: Create SURF Detector
        //SURF(double hessianThreshold, int nOctaves=4, int nOctaveLayers=2, bool extended=true, bool upright=false );
        Ptr<SURF> SURFdetector = SURF::create(3000, 4, 2, true, false);
        detector = SURFdetector;
    } else if (desc == "3") {
//        FREAK(bool orientationNormalized = true, bool scaleNormalized = true,
//              float patternScale = 22.0f, int nOctaves = 4,
//                const std::vector< int > &selectedPairs = std::vector< int >())
        //Ptr<FREAK> FREAKdetector = FREAK::create(true, true, 22, 4);
        Ptr<AKAZE> FREAKdetector = AKAZE::create();
        detector = FREAKdetector;
        binary = true;
    } else {
        std::cerr << "Usage: ./OPENCV_DESCRIPTORS descriptor" << std::endl;
        std::cerr << "\tdescriptor: \t1: SIFT." << std::endl;
        std::cerr << "\t \t2: SURF." << std::endl;
        return 1;
    }

    //--Step 0: Create SURF_GPU Detector - Check for OpenCV 4.0 documentation
    //SURF_GPU(double hessianThreshold, int nOctaves=4, int nOctaveLayers=2, bool extended=true, bool upright=false );
    //Ptr<cuda::SURF_CUDA> SURFdetector = cuda::SURF_CUDA::create(3000, 4, 2, true, false);
    //detector = SURFdetector;

    vid.open(0);

    bool first = true;
    int i = 0, wait_i = 40;
    Mat img1, img2;

    std::vector<KeyPoint> keypoints_1, keypoints_2;
    Mat img_keypoints_1;
    Mat img_keypoints_2;
    Mat descriptors1, descriptors2;

    Ptr<DescriptorMatcher> matcher;
    if(binary) {
         Ptr<BFMatcher> bf = BFMatcher::create(NORM_HAMMING);
         matcher = bf;
    } else
        matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);

    while(1) {
        if(i < wait_i) {
            ++i;
            continue;
        }

        //Get first image (reference) when spacebar is pressed:
        if(first) {
            vid >> img1;
            imshow("Video", img1);

            //detectKeypoints(detector, img1, keypoints_1, img_keypoints_1);
            if(binary)
                computeBinaryDescriptors(detector, img1, keypoints_1,
                               descriptors1, img_keypoints_1);
            else
                computeDescriptors(detector, img1, keypoints_1,
                               descriptors1, img_keypoints_1);
            c=cv::waitKey(10);
            if(c != -1)
                if((uchar)c == ' ') {
                    first = false;
                    continue;
                } else
                    break;
            continue;
        }

        //Take the frame for comparison
        vid >> img2;

        //detectKeypoints(detector, img2, keypoints_2, img_keypoints_2);
        if(binary)
            computeBinaryDescriptors(detector, img2, keypoints_2,
                           descriptors2, img_keypoints_2);
        else
            computeDescriptors(detector, img2, keypoints_2,
                           descriptors2, img_keypoints_2);

        //--Show detected (drawn) keypoints
        Mat one = collate(img_keypoints_1, img_keypoints_2);
        Mat half_one;

        //Step 4: Do matching
        std::vector< std::vector<DMatch> > knn_matches;
        matcher->knnMatch( descriptors1, descriptors2, knn_matches, 2 );

        //-- Filter matches using the Lowe's ratio test
        const float ratio_thresh = 0.7f;
        std::vector<DMatch> good_matches;
        for (size_t i = 0; i < knn_matches.size(); i++)
        {
            //std::cout << "Match " << i << ":\n"
            //          << "\timgIdx: " << knn_matches[i][0].imgIdx
            //          << "\n\tqueryIdx: " << knn_matches[i][0].queryIdx
            //          << "\n\timgIdx: " << knn_matches[i][0].trainIdx << std::endl;

            if(binary)
                good_matches.push_back(knn_matches[i][0]);
            else if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance) {
                good_matches.push_back(knn_matches[i][0]);
            }
        }

        drawMatches(one, good_matches, keypoints_1, keypoints_2);

        cv::resize(one, half_one, Size(2*one.cols/3,2*one.rows/3));
        imshow("Matched Descriptors (img1 -> img2)", half_one);

        //-- Draw matches
        //        Mat img_matches;
        //        drawMatches( img1, keypoints1, img2, keypoints2, good_matches, img_matches, Scalar::all(-1),
        //                     Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

        keypoints_2.clear();

        c=cv::waitKey(10);
        if(c != -1)
            if((uchar)c == ' ') {
                img2.copyTo(img1);
                keypoints_1.clear();
                if(binary)
                    computeBinaryDescriptors(detector, img1, keypoints_1,
                                   descriptors1, img_keypoints_1);
                else
                    computeDescriptors(detector, img1, keypoints_1,
                                   descriptors1, img_keypoints_1);
            } else
                break;
    }

    vid.release();
    return 0;

}
