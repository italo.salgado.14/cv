#-------------------------------------------------
#
# Project created by QtCreator 2016-08-08T14:54:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OPENCV_BASICO
TEMPLATE = app

LIBS += `pkg-config \
    opencv \
    --cflags \
    --libs`

SOURCES += main.cpp

HEADERS  +=

FORMS    += mainwindow.ui
