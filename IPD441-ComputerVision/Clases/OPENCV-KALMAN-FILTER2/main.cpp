#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"
#include <iostream>
#include <QInputDialog>
#include <QMainWindow>
#include <QApplication>
#include <fstream>


#define drawCross( center, color, d )                                 \
line( img, Point( center.x - d, center.y - d ), Point( center.x + d, center.y + d ), color, 2, cv::LINE_AA, 0); \
line( img, Point( center.x + d, center.y - d ), Point( center.x - d, center.y + d ), color, 2, cv::LINE_AA, 0 )

using namespace cv;
using namespace std;

bool pressed = false;
int min_d2 = 20*20;

vector<cv::Point> points;
Mat img(2000, 2000, CV_8UC3);

void drawLast(cv::Point &mousePos) {
    cv::circle(img, mousePos, 5, cv::Scalar(255,0,0),2);
    if(points.size()>1) {
        cv::Point prevPos = points[points.size()-2];
        cv::circle(img, prevPos, 5, cv::Scalar(0,255,0),2);
        line(img, prevPos, mousePos, Scalar(255,0,255), 2);
    }
    cv::imshow("Kalman Filter", img);

}

void drawLast(cv::Point &prevPos, cv::Point &mousePos) {
    line(img, prevPos, mousePos, Scalar(255,0,255), 2);
    cv::circle(img, prevPos, 5, cv::Scalar(0,255,0),2);
    cv::circle(img, mousePos, 5, cv::Scalar(255,0,0),2);
    cv::imshow("Kalman Filter", img);

}



void pressCallBack(int event, int x, int y, int flags, void* userdata) {
    std::cout << "(" << x << ", " << y << ")" << endl;
    if  ( event == cv::EVENT_LBUTTONDOWN ) {
        cv::Point mousePos;
        mousePos.x = x;
        mousePos.y = y;
        points.push_back(mousePos);
        drawLast(mousePos);
        pressed = true;
    } else if ( event == cv::EVENT_LBUTTONUP ) {
        pressed = false;
    }

    if(pressed) {
        cv::Point prevPos = points.back();
        int d2 = (prevPos.x - x)*(prevPos.x - x) + (prevPos.y - y)*(prevPos.y - y);
        if(d2 > min_d2) {
            cv::Point mousePos;
            mousePos.x = x;
            mousePos.y = y;
            points.push_back(mousePos);
            drawLast(prevPos, mousePos);

        }
    }


    std::cout << "(" << x << ", " << y << "):" << (pressed? "pressed":"released") << endl;

}

template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

void insertPoint(int x, int y) {
    cv::Point mousePos;
    mousePos.x = x;
    mousePos.y = y;
    points.push_back(mousePos);
    drawLast(mousePos);
    waitKey(20);
}


int main(int argc, char *argv[]) {
    QApplication *a = new QApplication(argc, argv);
    QMainWindow w;
    KalmanFilter KF(4, 2, 0); //#estado,#medición,#param.control

    //Create windows
    cv::namedWindow("Kalman Filter", 1);


    img = cv::Scalar(255,255,255);
    cv::imshow("Kalman Filter", img);

    if(argc == 1) {
        //Set the callback functions for left mouse event
        cv::setMouseCallback("Kalman Filter", pressCallBack, NULL);

        while(waitKey() != '1');
    } else {
        std::ifstream ifs (argv[1], std::ifstream::in);
        char line[1000];
        ifs.getline(line, 1000);
        while(strcmp(line, "") != 0) {
            std::string sline = line;
            std::vector<std::string> s = split(sline, '\t');
            std::cout << s[0] << ";" << s[1] << std::endl;
            insertPoint(atoi(s[0].c_str()),atoi(s[1].c_str()));
            ifs.getline(line, 1000);
        }

          ifs.close();
    }

    if(points.size() == 0)
        return 0;

    std::cout << "Sample size: " << points.size() << endl;
    std::cout << "Kalman Filter Phase..." << endl;

    // intialization of KF...#F
    KF.transitionMatrix = (Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
    Mat_<float> measurement(2,1); measurement.setTo(Scalar(0));

    int i = 0;
    cv::Point p = points[i++];
    measurement(0) = p.x; //x_i
    measurement(1) = p.y;

    KF.statePre.at<float>(0) = p.x; //a_(i-1)
    KF.statePre.at<float>(1) = p.y;
    KF.statePre.at<float>(2) = 0;
    KF.statePre.at<float>(3) = 0;
    KF.statePost.at<float>(0) = p.x; //a_(i)
    KF.statePost.at<float>(1) = p.y;
    KF.statePost.at<float>(2) = 0;
    KF.statePost.at<float>(3) = 0;

    setIdentity(KF.measurementMatrix); //H
    setIdentity(KF.processNoiseCov, Scalar::all(1e-4)); //Q_i
    setIdentity(KF.measurementNoiseCov, Scalar::all(1)); //R_i
    setIdentity(KF.errorCovPre, Scalar::all(.1)); //P_(i-1)
    setIdentity(KF.errorCovPost, Scalar::all(.1)); //P_i
    // Image to show mouse tracking
    vector<Point> mousev,kalmanv;
    mousev.clear();
    kalmanv.clear();
    kalmanv.push_back(p);
    while(i < points.size()) {
        // First predict, to update the internal statePre variable
        Mat prediction = KF.predict();
        Point predictPt(prediction.at<float>(0),prediction.at<float>(1));

        // Get mouse point
        p = points[i++];
        measurement(0) = p.x;
        measurement(1) = p.y;

        // The update phase
        Mat estimated = KF.correct(measurement);

        Point statePt(estimated.at<float>(0),estimated.at<float>(1));
        Point measPt(measurement(0),measurement(1));
        // plot points

        mousev.push_back(measPt);
        kalmanv.push_back(statePt);
        drawCross( statePt, Scalar(0,0,0), 5 );
        drawCross( measPt, Scalar(0,0,255), 5 );

        if(kalmanv.size() > 1)
            line(img, kalmanv[kalmanv.size()-2], kalmanv[kalmanv.size()-1], Scalar(0,155,255), 2);
        cv::imshow("Kalman Filter", img);
        cv::waitKey(20);
    }

    if(cv::waitKey()=='2') {
        QString text = QInputDialog::getText(&w,"Save Points Sequence", "File Name",
                                             QLineEdit::Normal, "points.txt");
        if(text != "") {
            std::ofstream ofs(text.toStdString(), std::ofstream::out);
            for(int i=0; i<points.size(); i++)
                ofs << points[i].x << "\t" << points[i].y << "\n";
            ofs.close();
        }
    }

    return a->exec();
}
