#include<iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

cv::Mat histogramaRegion(cv::Mat BGR, int index, cv::Mat mov, cv::Mat MagSobel, int bins) {
    cv::Mat histo(bins, bins, CV_32FC1), YUV;
    histo = 0.0;
    cv::cvtColor(BGR, YUV, cv::COLOR_BGR2YCrCb);
    int i, j, k, bsize = 255/bins, counter = 0,
        rows = YUV.rows, cols = YUV.cols, step = YUV.step;
    uchar *data = YUV.data;
    uchar U, V;
    for(i=0; i<rows; ++i)
        for(j=0, k=0; j<cols; ++j, k+=3)
            if(mov.at<int>(i,j) == index) {
                counter++;
                U = data[i*step + k + 1];
                V = data[i*step + k + 2];
                histo.at<float>(U/bsize, V/bsize) += 1.0 - MagSobel.at<float>(i,j);
            }
    histo /= counter;
    return histo;
}


int main(int argc, char *argv[]) {

    if(argc != 3) { //Recuerde: nombre del programa se toma como argumento tambien
        cerr << "Usage: ./OPENCV_THRESHOLDING image type" << endl;
        cerr << "\ttype: 0:Normal, 1:Otsu, 2:adaptive" << endl;
        return 1;
    }

    int type = std::atoi(argv[2]);
    if(type<0 || type>3) {
        cerr << "Usage: ./OPENCV_THRESHOLDING image type" << endl;
        cerr << "\ttype: 0:Normal, 1:Otsu, 2:adaptive mean, 3: adaptive gaussian" << endl;
        return 1;
    }

    cv::Mat M, th, M1 = cv::imread( argv[1], 1 );
    if(M1.empty()) {
        cerr << "Error reading image " << argv[1] << endl;
        return 1;
    }

    //cv::resize(M1, M, cv::Size(M1.cols/2,M1.rows/2));
    int threshold_value = 128;
    cv::imshow("Input", M1);
    cv::cvtColor(M1, M, cv::COLOR_BGR2GRAY);
    if(type == 0)           //thresholding
        threshold_value = cv::threshold(M, th, 128, 255, cv::THRESH_BINARY);
    else if(type == 1)      //thresholding con  otsu
        threshold_value = cv::threshold(M, th, 128, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    else if(type == 2)      //threshholding adaptivo
        cv::adaptiveThreshold(M, th, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 11, 0);
    else                    //threshold gaussiano
        cv::adaptiveThreshold(M, th, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 0);


    cv::imshow("Result", th);

    /// Establish the number of bins
    int histSize = 256;

    /// Set the ranges ( for B,G,R) )
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    bool uniform = true; bool accumulate = false;
    cv::Mat hist;

    if(type <= 1) {
        /// Compute the histograms:
        cv::calcHist(&M, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);

        // Draw the histograms for B, G and R
        int hist_w = 512; int hist_h = 400;
        int bin_w = cvRound( (double) hist_w/histSize );

        cv::Mat histImage( hist_h, hist_w, CV_8UC3, cv::Scalar( 0,0,0) );

        /// Normalize the result to [ 0, histImage.rows ]
        cv::normalize(hist, hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat() );

        /// Draw
        for( int i = 1; i < histSize; i++) {
            cv::line(histImage, cv::Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ) ,
                            cv::Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
                            cv::Scalar(255, 255, 255), 2, 8, 0  );
        }

        cv::line(histImage, cv::Point( bin_w*(threshold_value-1), 0),
                        cv::Point( bin_w*(threshold_value-1), hist_h-1),
                        cv::Scalar(0, 0, 255), 2, 8, 0  );


        cv::imshow("Histogram", histImage);
    }
    cv::waitKey(0);



    return 0;
}
