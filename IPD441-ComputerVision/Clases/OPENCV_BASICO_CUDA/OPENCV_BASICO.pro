#-------------------------------------------------
#
# Project created by QtCreator 2016-08-08T14:54:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OPENCV_BASICO
TEMPLATE = app

LIBS += `pkg-config \
    opencv \
    --cflags \
    --libs`

HEADERS  +=

FORMS    += mainwindow.ui

DISTFILES +=

#Files to be compiled by g++
SOURCES +=

#CUDA + OPENCV in QTCREATOR: Based on https://cudaspace.wordpress.com/2012/07/05/qt-creator-cuda-linux-review/

#Files to be compiled by NVCC
CUDA_SOURCES += main.cpp

NVCCFLAGS     = --compiler-options -fno-strict-aliasing -use_fast_math --ptxas-options=-v

# Prepare the extra compiler configuration (taken from the nvidia forum - i'm not an expert in this part)
CUDA_INC = $$join(INCLUDEPATH,' -I','-I',' ')

# Path to cuda toolkit install
CUDA_DIR      = /usr/local/cuda

# GPU architecture
CUDA_ARCH     = sm_20                # Adjust with your compute capability

#cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -arch=$$CUDA_ARCH -c $$NVCCFLAGS \
cuda.commands = $$CUDA_DIR/bin/nvcc -m64 -O3 -c $$NVCCFLAGS \
                $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
# nvcc error printout format ever so slightly different from gcc
# http://forums.nvidia.com/index.php?showtopic=171651

cuda.dependency_type = TYPE_C # there was a typo here. Thanks workmate!
cuda.depend_command = $$CUDA_DIR/bin/nvcc -O3 -M $$CUDA_INC $$NVCCFLAGS ${QMAKE_FILE_NAME}

cuda.input = CUDA_SOURCES
cuda.output = ${OBJECTS_DIR}${QMAKE_FILE_BASE}_cuda.o
# Tell Qt that we want add more stuff to the Makefile
QMAKE_EXTRA_COMPILERS += cuda
