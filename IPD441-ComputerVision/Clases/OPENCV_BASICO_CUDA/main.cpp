#include<iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


using namespace std;

int main(int argc, char *argv[]) {

    if(argc == 1) {
        cerr << "Usage: ./OPENCV_BASICO image" << endl;
        return 1;
    }

    cv::Mat M = cv::imread( argv[1], 1 );
    cv::Mat roi(M, cv::Rect(10,10,200,200));
    // Llenar el ROI con (0,255,0) (verde);
    // imagen original de 320x240 se modifica
    //roi = cv::Scalar(0,255,0);
    int i, j, cols = M.cols,
    rows = M.rows;
    int depth = M.depth(),
    channels = M.channels(),
    step = M.step;
    uchar *data = M.data;
    std::cout << "Cols: " << cols << std::endl;
    std::cout << "Rows: " << rows << std::endl;
    std::cout << "channels: " << channels << std::endl;
    std::cout << "step: " << step << std::endl;
    for(i = 0; i < rows; i++) {
    for(j = 0; j < cols; j++) {
        data[i*step + j*channels + 1]
        = data[i*step + j*channels + 2] = 0;
        }
    }

    if(M.empty()) {
        cerr << "Error reading image " << argv[1] << endl;
        return 1;
    }

    cv::imshow("Input", M);

    cv::Mat M = img;
    int i, j, cols = M.cols,
              rows = M.rows;

    for(i = 0; i < rows; i++) {
        for(j = 0; j < cols; j++) {
            cv::Vec3b pixel = M.at<unsigned char>(i,j);
            cout << "(i,j): " << i << "," << j << ")" << endl;
            cout << "B: "   << (int)pixel.val[0]
                 << "; G: " << (int)pixel.val[1]
                 << "; R: " << (int)pixel.val[2] << endl;
        }
    }



    vector<cv::Mat> bgr_planes;
    cv::split( img, bgr_planes );
    cv::imshow("Red", bgr_planes[2]);
    cv::imshow("Green", bgr_planes[1]);
    cv::imshow("Blue", bgr_planes[0]);
    bgr_planes[0] = 0;
    bgr_planes[2] = 0;
    cv::Mat verde;
    merge(bgr_planes, verde);
    cv::imshow("Verde", verde);


    //recorrer una imagen con iteradores
    cv::MatIterator_<cv::Vec3b> it, end;

    cv::Mat dst;
    img.copyTo(dst);

    for (it = dst.begin<cv::Vec3b>(), end = dst.end<cv::Vec3b>(); it != end; it++) {
        (*it)[0] = 0;
        //(*it)[1] = (*it)[1];
        (*it)[2] = 0;
    }

    //una copia de la imagen general operada con iteradores
    cv::Mat dst;
    img.copyTo(dst);

    cv::Mat_<cv::Vec3b> _I = dst;

    for(int i = 0; i < dst.rows; ++i)
        for(int j = 0; j < dst.cols; ++j) {
            _I(i,j)[0] = 0;
            //_I(i,j)[1] = _I(i,j)[1];
            _I(i,j)[2] = 0;
        }
    dst = _I;

    cv::imshow("Verde", dst);

    // operaciones sobre la imagen
    cv::Mat dst;
    cv::resize(img, dst, cv::Size(img.cols/2, img.rows/2));
    cv::imshow("Chica", dst);

    dst = cv::Scalar(0,128,0); //e.g. Imagen verde
    dst += 10; //Aumenta todos los canales y pixeles en 10
    cv::imshow("Otra", dst);


    cv::waitKey(0);



    return 0;
}
