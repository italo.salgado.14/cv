#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main( int argc, char** argv ) {
    Mat src;
    // the first command-line parameter must be a filename of the binary
    // (black-n-white) image
    if( argc != 2 || !(src=imread(argv[1])).data)
        return -1;

    Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);

    //src = src > 1;
    imshow( "Original", src ); //la imagen original

    Mat src2(src.rows, src.cols, CV_8UC1); //una imagen del mismo tamano pero de 1 canal

    cvtColor(src, src2, COLOR_BGR2GRAY); //1 solo canal de gris

    //aplicar umbralizacion, con 0 es BINARY THRESHOLD
    Mat src3;
    cv::threshold(src2, src3, 128, 255, 0);
    imshow( "Umbralizada", src3); //luego del tresholding

    //construir elemento estructurante de 3x3 recto
    Mat e = getStructuringElement(MORPH_RECT, Size(3,3));

    //la forma mas simple de obtener bordes de imagenes binarias
    Mat src4;
    cv::erode(src3, src4, e); //erosion pequena
    Mat src5;
    bitwise_xor(src3, src4, src5); //bordes binarios, no es una resta
                                                //propiamente tal, si no que entrega
                                                //valor positivo donde existan valores disstintos
    imshow( "Contorno", src5);

    vector<vector<Point> > contours; //un vector de vectores de puntos para almacenar los contornos
    //hierarchy: ordenar la informacion de contornos en una jerarquia
    vector<Vec4i> hierarchy; //cuatro vectores de valores enteros

    //aproximacion simple con todos los contornos
    findContours( src5, contours, hierarchy,
        RETR_TREE, CHAIN_APPROX_SIMPLE );

    // iterate through all the top-level contours,
    // draw each connected component with its own random color
    for(int idx = 0; idx >= 0; idx = hierarchy[idx][0] ) {
        Scalar color( rand()&255, rand()&255, rand()&255 ); //colores aleatorios entre 0 y 255
        //un color-contorno por cada indice
        drawContours( dst, contours, idx, color, 1, 8, hierarchy, 0);
    }
    imshow( "Contornos", dst );

    Mat dst2 = Mat::zeros(src.rows, src.cols, CV_8UC3);
    //indice -1 es para mostrarlos todos
    drawContours(dst2, contours, -1, Scalar(255,255,255), 1, 8, hierarchy);
    imshow( "Todos", dst2);

    /*
     * contando cantidad de puntos en el contorno
     * cuidado con la aproximacion, esto puede no funcionar
     */
    int max_i = -1;
    unsigned int max = 0;
    for(int i=0; i<contours.size(); i++) {
        if(contours[i].size() > max) {
            max = contours[i].size();
            max_i = i; //maximo a nivel de cantidad de puntos
        }
    }

    //aqui trabajamos solo con el contorno mas grande
    Mat dst3 = Mat::zeros(src.rows, src.cols, CV_8UC3);
    if(max > 0) { //Some contour found
        std::vector<Point> poly;
        //Ramer-Douglas-Peucker poligonal de OpenCV, con un error epsilon
        //aproximar los valores del borde. A mayor epsilon menor detalle se mantiene,
        // mayor es la aproximacion, eliminacion de detalles superfluos
        approxPolyDP(contours[max_i], poly, 70, true);
        std::cout << "SiZE: " << poly.size() << std::endl;
        std::vector< std::vector<Point> > mpoly;
        mpoly.push_back(poly);
        drawContours(dst3, mpoly, -1, Scalar(255,255,255), 3, 8);

    }
    imshow( "Aproximacion Poligonal", dst3);

    //generar una envoltura convexa
    Mat dst4 = Mat::zeros(src.rows, src.cols, CV_8UC3);
    std::vector<Point> p;
    //encontrar la minima forma convexa asociada a la figura
    cv::convexHull(contours[max_i], p); //envolver el contorno maximo
    std::vector<std::vector<Point>> pp;
    pp.push_back(p);
    drawContours(dst4, pp, -1, Scalar(255,255,255), 2, 8);
    imshow("Convexe Hull", dst4);

    waitKey(0);

    return 0;
}
