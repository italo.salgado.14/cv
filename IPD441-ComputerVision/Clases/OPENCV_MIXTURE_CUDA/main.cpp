#include<iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/*
 * PPT 5 de CV
 */

using namespace std;

void paintRectangles(cv::Mat &img, std::map<int, cv::Rect> &bboxes) {
    std::map<int, cv::Rect>::iterator it, it_end = bboxes.end();
    for(it = bboxes.begin(); it != it_end; it++) {
        cv::rectangle(img, it->second, cv::Scalar(0,0,255), 2);
    }
}

//busca los minimos y los maximos en las posiciones de los pixeles, en base a eso generar
//luego una box englobante que agrupe esos pixeles
void getBlobs(cv::Mat labels, std::map<int, cv::Rect> &bboxes) {
    int r = labels.rows, c = labels.cols;
    int label, x, y;
    bboxes.clear();
    for(int j=0; j<r; ++j)
        for(int i=0; i<c; ++i) {
            label = labels.at<int>(j,i);
            if(label > 0) {
                if(bboxes.count(label) == 0) { //New label
                    cv::Rect r(i,j,1,1);
                    bboxes[label] = r;
                } else { //Update rect
                    cv::Rect &r = bboxes[label];
                    x = r.x + r.width  - 1;
                    y = r.y + r.height - 1;
                    if(i < r.x) r.x = i;
                    if(i > x) x = i;
                    if(j < r.y) r.y = j;
                    if(j > y) y = j;
                    r.width = x - r.x + 1;
                    r.height = y - r.y + 1;
                }
            }
        }
}

//#include <media/v4l2-ctrls.h>


int main(int argc, char *argv[]) {

    cv::VideoCapture vid;
    vid.open(0);
    vid.set(cv::CAP_PROP_AUTO_EXPOSURE, 0);
    vid.set(cv::CAP_PROP_EXPOSURE, -7.0);
//    vid.open("TEST01.mp4");
//    cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7,7)));

/*     struct v4l2_control ctrl;
     //cap.read(src);
     ctrl.id = V4L2_CID_EXPOSURE_AUTO;
     ctrl.value = V4L2_EXPOSURE_MANUAL;
     if(v4l2_ioctl(descriptor, VIDIOC_S_CTRL, &ctrl))
         std::cout << "Error disabling auto exposure\n";
     ctrl.id = V4L2_CID_EXPOSURE_ABSOLUTE;
     ctrl.value = 100;
     if(v4l2_ioctl(descriptor, VIDIOC_S_CTRL, &ctrl))
         std::cout << "Error setting exposure\n";
*/

    if(!vid.isOpened()) {
        cerr << "Error opening input." << endl;
        return 1;
    }

    cv::Mat img, bg, fg, labels;

    // ptr es un puntero compartido, cuando se deja de ocupar se libera
    cv::Ptr<cv::BackgroundSubtractorMOG2> mog;
    int historyMOG = 200; //Length of the history, no hace nada XD
    int nmixtures = 10; //Number of Gaussians Mixtures, numero de mezclas
    //que % de los modelos se considerara fondo
    //Threshold of data that should be accounted for by the background (Parámetro T)
    double backgroundRatio = 0.5;
    double learningRate = 0.005; //Learning Rate
    double varThreshold = 5; //Umbral segmentacion, umbral de la distancia


    bool bShadowDetection = false; //quiere o no detectar sombras - falla a veces
    mog = cv::createBackgroundSubtractorMOG2(historyMOG, varThreshold, bShadowDetection);
    mog->setNMixtures(nmixtures);
    mog->setBackgroundRatio(backgroundRatio);

/*
    mog->setVarThresholdGen(varThresholdGen);
    mog->setVarInit(fVarInit);
    mog->setVarMin(fVarMin);
    mog->setVarMax(fVarMax);
    mog->setComplexityReductionThreshold(fCT);
    mog->setDetectShadows(nShadowDetection);
    mog->setShadowThreshold(fTau);
    */


    bool first = true;
    int i=0,c;
    int esize = 25;

    while(1) {
        vid >> img;
        i++;
        if(i>20) { //descartar los primeros 20 frames
            if(first) { //guardar el primer frame, el 21, como background inicial
                first = false;
                img.copyTo(bg);
                mog->apply(bg, fg, learningRate);
            } else {

                mog->apply(img, fg, learningRate);

                //apertura y cierre morfologico para limpiar
                cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize,esize)));
                cv::dilate(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize,esize)));

                cv::dilate(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize,esize)));
                cv::erode(fg, fg, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(esize,esize)));

                //componentes conectados: pasele la imagen de foreground, obtener imagen de etiquetas
                //labels es un cv::Mat
                cv::connectedComponents(fg, labels, 8, CV_32S);
                std::map<int, cv::Rect> bboxes;
                getBlobs(labels, bboxes); //genera los blobs
                paintRectangles(img, bboxes);


                imshow("Video", img);
                imshow("Background", bg);
                imshow("Foreground", fg);


            }
        }
        if( (c=cv::waitKey(10)) != -1)
            break;
    }

    mog->~Algorithm();

    vid.release();

    cv::Mat gs(1, 256, CV_8UC3), YCrCb;
    for(int i=0; i<256; i++) {
        gs.data[3*i] = gs.data[3*i + 1] = gs.data[3*i + 2] = i;
    }
    cv::cvtColor(gs, YCrCb, cv::COLOR_BGR2YCrCb);

    for(int i=0; i<256; i++) {
        cout << i << ": Y=" << (int)YCrCb.data[3*i] << "; Cr=" << (int)YCrCb.data[3*i + 1]
             <<"; Cb="<< (int)YCrCb.data[3*i + 2] << ";" << endl;
    }

    return 0;
}



