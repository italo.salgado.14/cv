#include<iostream>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

using namespace std;
using namespace cv;

/*
 * DESDISTOSION -> PROBLEMA LINEAL
 *
 *
 *  * SUPESTO: LA ALTURA SUPUESTA ES DE 0 --> Z=0
 * eso me permite la relacion punto a punto. Ese supuesot debe tomarse
 * que pasaria si la altura no es 0 (z != 0)? el componente 2 no sería 0 en matriz Pr
 */

//Global fundamental matrix, for callback
cv::Mat H;
cv::Mat img_proy, out;

void projectionCallBack(int event, int x, int y, int flags, void* userdata) {

    int rad = 5;
    img_proy.copyTo(out); //la imagen desdistorsionada
    //copio un circulo cada vez que hay un evento de mouse
    cv::circle(out, cv::Point(x,y),rad,cv::Scalar(0,255,0));

    //vector de 3 elementos, x-y-1
    cv::Mat p2D = cv::Mat::ones(3, 1, CV_64FC1);
    p2D.at<double>(0,0) = x; //guardo el x del mouse
    p2D.at<double>(1,0) = y; //guardo el y del mouse

    //multiplicamos entonces el punto mouse por la derecha, con la matriz de homografía
    //obtenemos entonces un punto p 3D de la escena con z=0.
    cv::Mat p = H*p2D;
    //el z no lo muestro porque ya me lo se, proyecto el x-y que me interesan
    double xx = p.at<double>(0)/p.at<double>(2), yy = p.at<double>(1)/p.at<double>(2);

    //le muestro por pantalla, el punto en el mundo
    std::cout << xx << "; " << yy << std::endl;
    std::string s = "(" + std::to_string(xx) + ";" + std::to_string(yy) + ")";

    //pegarle como texto a la imagen la coordenada
    cv::putText(out, s, cv::Point(10,40), cv::FONT_HERSHEY_DUPLEX, 1, cv::Scalar(0,255,0));
    //mostrar
    cv::imshow("Undistorted Image", out);

}


int main(int argc, char *argv[]) {

    cv::VideoCapture vid;

    vid.open(0);

    if(!vid.isOpened()) {
        cerr << "Error opening input." << endl;
        return 1;
    }

    cv::Mat img, img_cpy, gray;
    int c;
    std::vector<Point3f> obj;
    int board_w = 12;
    int board_h = 10;
    int board_n = board_w * board_h;
    cv::Size patternsize = cv::Size(board_w, board_h);
    for(int j=0;j<board_n;j++)
        obj.push_back(Point3f(j/board_w, j%board_w, 0.0f));

    TermCriteria criteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.1);
    int numPerspectiveFrames = 0;
    int neededPerspectiveFrames = 40;
    int currentPerspectiveFrames = 0;
    int totalPerspectiveFrames = 5;

    std::vector<std::vector<Point3f> > object_points;
    std::vector<std::vector<Point2f> > image_points;
    bool perspectiveOK = false;
    cv::Mat intrinsic_matrix(3,3,CV_64FC1);
    cv::Mat distortion_coeffs = cv::Mat::zeros(8,1,CV_64FC1);
    std::vector<Mat> rvecs;
    std::vector<Mat> tvecs;
    std::vector<Point2f> corners;

    H = cv::Mat::ones(3,3, CV_64FC1);

    while(true) {
        vid >> img;
        cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

        img.copyTo(img_cpy);

        if(!perspectiveOK) {
            numPerspectiveFrames++;
            if(numPerspectiveFrames == neededPerspectiveFrames) {
                numPerspectiveFrames = 0;

                if(cv::findChessboardCorners(gray, patternsize, corners, cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FILTER_QUADS)) {
                    currentPerspectiveFrames++;
                    std::cout << "Found corners " << currentPerspectiveFrames << "times!" << std::endl;

                    cv::cornerSubPix(gray, corners, Size(11, 11), Size(-1, -1), criteria);
                    cv::drawChessboardCorners(img_cpy, patternsize, cv::Mat(corners), true);

                    image_points.push_back(corners);
                    object_points.push_back(obj);

                    cv::imshow("Detected chessboard", img_cpy);

                    if(currentPerspectiveFrames == totalPerspectiveFrames) {
                        perspectiveOK = true;
                        intrinsic_matrix.ptr<float>(0)[0] = 1;
                        intrinsic_matrix.ptr<float>(1)[1] = 1;
                        //CALIBRATE THE CAMERA!

                        cv::calibrateCamera(object_points, image_points,
                            img.size(),
                            //intrinsic_matrix, distortion_coeffs, rvecs, tvecs,  cv::CALIB_RATIONAL_MODEL | cv::CALIB_ZERO_TANGENT_DIST | cv::CALIB_FIX_K4 | cv::CALIB_FIX_K5 | cv::CALIB_FIX_K6); //cv::CALIB_FIX_ASPECT_RATIO
                            intrinsic_matrix, distortion_coeffs, rvecs, tvecs,  cv::CALIB_RATIONAL_MODEL | cv::CALIB_THIN_PRISM_MODEL); //cv::CALIB_FIX_ASPECT_RATIO

                            std::cout << "Intrinsic:" << intrinsic_matrix << std::endl;
                            std::cout << "Distorsion:" << distortion_coeffs << std::endl;

                            //Draw something nice
                            cv::Mat imageUndistorted;
                            cv::undistort(img_cpy, imageUndistorted, intrinsic_matrix, distortion_coeffs);
                            imageUndistorted.copyTo(img_proy);
                            cv::imshow("Current Frame Undistorted", imageUndistorted);


                    }
                } else if(currentPerspectiveFrames > 0) { //Show last corners
                        cv::drawChessboardCorners(img_cpy, patternsize, cv::Mat(corners), true);
                }
            }
            cv::imshow("Current Frame", img_cpy);

        } else {
            cv::drawChessboardCorners(img_cpy, patternsize, cv::Mat(corners), true);
            cv::imshow("Current Frame", img_cpy);
        }

        c=cv::waitKey(10);
        if(c != -1)
            break;
    }
    vid.release();

    //ya tengo las perspectiva ultima ya esta ok, la version v1 tiene las explicacion de como lo hace
    //ya calibramos la perspectiva
    if(perspectiveOK) {
        cv::Mat rvec = rvecs.back(), R; //tomo el ultimo vector de rotacion
        cv::Mat tvec = tvecs.back(); //y el ultimo vector de traslacion del conjunto de todas las veces que mostre el calibrador
        cv::Mat P, Pr = cv::Mat::zeros(3,3,CV_64FC1), RT = cv::Mat::zeros(3,4,CV_64FC1);

        //obtener una homografia desde la matriz perspectiva
        H = cv::Mat::zeros(3,3,CV_64FC1);    //intrinsic_matrix(3,3,CV_64FC1);

        //For height 0, third column is suppressed for homography
        cv::Rodrigues(rvec, R); //esto permite convertir el vector de rotacion en la matriz 3x3 discutida

        //construir la matriz de rotacion traslacion
        //trabajamos el problema lineal, trabajando con el problema desdistorsionado
        RT.at<double>(0,0) = R.at<double>(0,0); //rotacion
        RT.at<double>(0,1) = R.at<double>(0,1);
        RT.at<double>(0,2) = R.at<double>(0,2);
        RT.at<double>(0,3) = tvec.at<double>(0); //3x4, traslacion
        RT.at<double>(1,0) = R.at<double>(1,0);
        RT.at<double>(1,1) = R.at<double>(1,1);
        RT.at<double>(1,2) = R.at<double>(1,2);
        RT.at<double>(1,3) = tvec.at<double>(1);
        RT.at<double>(2,0) = R.at<double>(2,0);
        RT.at<double>(2,1) = R.at<double>(2,1);
        RT.at<double>(2,2) = R.at<double>(2,2);
        RT.at<double>(2,3) = tvec.at<double>(2);

        //para obtener la matriz perspectiva
        P = intrinsic_matrix * RT; //multiplicar la matriz intrinseca por la matriz de rotacion-traslacion
        std::cout << "P: " << P << std::endl;
        //Skip 3rd column as z=0:
        //reduzco el P a las coordenadas. el z=0 siempre, me salto el 2 siempre.
        Pr.at<double>(0,0) = P.at<double>(0,0);
        Pr.at<double>(0,1) = P.at<double>(0,1);
        Pr.at<double>(0,2) = P.at<double>(0,3);
        Pr.at<double>(1,0) = P.at<double>(1,0);
        Pr.at<double>(1,1) = P.at<double>(1,1);
        Pr.at<double>(1,2) = P.at<double>(1,3);
        Pr.at<double>(2,0) = P.at<double>(2,0);
        Pr.at<double>(2,1) = P.at<double>(2,1);
        Pr.at<double>(2,2) = P.at<double>(2,3);
        //Pr entonces será entonces la homografia entre puntos 3D y 2D

        /*
         * si tengo una altura h en vez de pensar que es 0. ese valor de h sera el z fijo
         * vea ultima ppt de stereo vision para ver la matriz
         *
         * double h = 0;
         * Pr.at<double>(0,2) = P.at<double>(0,2) * h +  P.at<double>(0,3);;
         * Pr.at<double>(1,2) = P.at<double>(2,2) * h +  P.at<double>(1,3);;
         * Pr.at<double>(2,2) = P.at<double>(2,2) * h +  P.at<double>(2,3);;
         */


        //si lo invierto obtengo la transformacion entre 2D y 3D
        H = Pr.inv();

        //Create windows
        cv::namedWindow("Undistorted Image", 1);

        //Set the callback functions for left mouse event
        //interaccion con frames con el mouse, vea la funcion projectionCallBack
        cv::setMouseCallback("Undistorted Image", projectionCallBack, NULL);
        cv::imshow("Undistorted Image", img_proy);
        cv::waitKey(0);
    }

    return 0;
}



