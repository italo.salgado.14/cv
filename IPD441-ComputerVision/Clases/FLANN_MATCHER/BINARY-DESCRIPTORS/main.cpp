#include <opencv2/features2d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <vector>
#include <iostream>

using namespace std;
using namespace cv;

const float inlier_threshold = 2.5f; // Distance threshold to identify inliers
const float nn_match_ratio = 0.8f;   // Nearest neighbor matching ratio

enum bdescriptors {
    B_AKAZE,
    B_FREAK,
    B_BRISK,
    B_ORB,
    B_SIFT,
    B_SURF
};


int main(int argc, char *argv[]) {

    bdescriptors bd = B_FREAK;
    std::string TO_IMAGE = "3";

    std::string s = "data/img" + TO_IMAGE + ".ppm";

    Mat img1 = imread("data/img1.ppm", IMREAD_GRAYSCALE);
    Mat img2 = imread(s, IMREAD_GRAYSCALE);

    Mat homography;
    FileStorage fs("data/H1to"+ TO_IMAGE + "p.yml", FileStorage::READ);
    fs.getFirstTopLevelNode() >> homography;

    vector<KeyPoint> kpts1, kpts2;
    Mat desc1, desc2;

    if(bd == B_AKAZE) {
        Ptr<AKAZE> bdesc = AKAZE::create();
        bdesc->detectAndCompute(img1, noArray(), kpts1, desc1);
        bdesc->detectAndCompute(img2, noArray(), kpts2, desc2);
    } else if(bd == B_FREAK) {
        Ptr<ORB> detector = ORB::create();
        detector->detect(img1, kpts1 );
        detector->detect(img2, kpts2 );
        Ptr<cv::xfeatures2d::FREAK> bdesc = cv::xfeatures2d::FREAK::create();
        bdesc->compute(img1, kpts1, desc1);
        bdesc->compute(img2, kpts2, desc2);
    } else if(bd == B_BRISK) {
        Ptr<BRISK> bdesc = BRISK::create();
        bdesc->detectAndCompute(img1, noArray(), kpts1, desc1);
        bdesc->detectAndCompute(img2, noArray(), kpts2, desc2);
    } else if(bd == B_ORB) {
        Ptr<ORB> bdesc = ORB::create();
        bdesc->detectAndCompute(img1, noArray(), kpts1, desc1);
        bdesc->detectAndCompute(img2, noArray(), kpts2, desc2);
    } else if(bd == B_SIFT) {
        Ptr<cv::SIFT> bdesc = cv::SIFT::create();
        bdesc->detectAndCompute(img1, noArray(), kpts1, desc1);
        bdesc->detectAndCompute(img2, noArray(), kpts2, desc2);
    } else if(bd == B_SURF) {
        Ptr<cv::xfeatures2d::SURF> bdesc = cv::xfeatures2d::SURF::create();
        bdesc->setHessianThreshold( 100 );
        bdesc->detectAndCompute(img1, noArray(), kpts1, desc1);
        bdesc->detectAndCompute(img2, noArray(), kpts2, desc2);
    }

    vector<KeyPoint> matched1, matched2, inliers1, inliers2;
    vector<DMatch> good_matches;

    if(bd == B_SIFT || bd == B_SURF) {
        //BFMatcher matcher;
        Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);

        std::vector< DMatch > matches;
        //matcher.match( desc1, desc2, matches);

        matcher->match( desc1, desc2, matches, 2);

        double max_dist = 0, min_dist = DBL_MAX;

        //-- Quick calculation of max and min distances between keypoints
        for( int i = 0; i < matches.size(); i++ ) {
            double dist = matches[i].distance;
            if( dist < min_dist ) min_dist = dist;
            if( dist > max_dist ) max_dist = dist;
        }

        for( int i = 0; i < matches.size(); i++ )
            if( matches[i].distance < nn_match_ratio*max_dist ) {
                matched1.push_back(kpts1[matches[i].queryIdx]);
                matched2.push_back(kpts2[matches[i].trainIdx]);
            }

    } else { //Binary matchers
        //BFMatcher matcher(NORM_HAMMING);
        Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);

        vector< vector<DMatch> > nn_matches;

        //matcher.knnMatch(desc1, desc2, nn_matches, 2);

        //Flann requiere que descriptores sean de 32 bits
        if(desc1.type()!=CV_32F)
            desc1.convertTo(desc1, CV_32F);
        if(desc2.type()!=CV_32F)
            desc2.convertTo(desc2, CV_32F);
        matcher->knnMatch(desc1, desc2, nn_matches, 2);
        std::cout << "Aca2" << std::endl;

        for(size_t i = 0; i < nn_matches.size(); i++) {
            DMatch first = nn_matches[i][0];
            float dist1 = nn_matches[i][0].distance;
            float dist2 = nn_matches[i][1].distance;

            if(dist1 < nn_match_ratio * dist2) {
                matched1.push_back(kpts1[first.queryIdx]);
                matched2.push_back(kpts2[first.trainIdx]);
            }
        }
    }

    for(unsigned i = 0; i < matched1.size(); i++) {
        Mat col = Mat::ones(3, 1, CV_64F);
        col.at<double>(0) = matched1[i].pt.x;
        col.at<double>(1) = matched1[i].pt.y;

        col = homography * col;
        col /= col.at<double>(2);
        double dist = sqrt( pow(col.at<double>(0) - matched2[i].pt.x, 2) +
                            pow(col.at<double>(1) - matched2[i].pt.y, 2));

        if(dist < inlier_threshold) {
            int new_i = static_cast<int>(inliers1.size());
            inliers1.push_back(matched1[i]);
            inliers2.push_back(matched2[i]);
            good_matches.push_back(DMatch(new_i, new_i, 0));
        }
    }

    Mat res;
    drawMatches(img1, inliers1, img2, inliers2, good_matches, res);
    imwrite("res.png", res);

    double inlier_ratio = inliers1.size() * 1.0 / matched1.size();
    cout << "Matching Results" << endl;
    cout << "*******************************" << endl;
    cout << "# Keypoints 1:                        \t" << kpts1.size() << endl;
    cout << "# Keypoints 2:                        \t" << kpts2.size() << endl;
    cout << "# Matches:                            \t" << matched1.size() << endl;
    cout << "# Inliers:                            \t" << inliers1.size() << endl;
    cout << "# Inliers Ratio:                      \t" << inlier_ratio << endl;
    cout << endl;

    return 0;
}
